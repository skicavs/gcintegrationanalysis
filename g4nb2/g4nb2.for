        PROGRAM g4nb2
C======================================================================
C     Driver for chi-squared minimization based on Numerical Recipes 
C        14.4, modified to fit chromatographic peaks with 4-coefficient
C        Gaussians plus a 2-coefficient baseline.  Output is A(1:ma),
C        the set of coefficients that gives the least-squares best fit,
C        where ma = 4 * (number of peaks) + 2.
C        The adjustable parameters A are: (m) amplitude  (m+1) center
C           (m+2) left width  (m+3) distortion=rightwidth/leftwidth ...
C           (ma-1) baseline slope  (ma) baseline offset.
C     This driver reads a conditions file "FCOND.TXT" which specifies
C        the filename of an x,y data file (dfile), the number of 
C        overlapping peaks to fit (NPKS), the number of x,y data 
C        (NPTS), the edges of the initial search for the peaks 
C        (j0(npks+1)), and lower and upper bounds Amin(1:ma), 
C        Amax(1:ma) on the values of the fitted coefficients A.
C     Then this driver calls MAINIO which 1) reads the x,y data file and
C        2) finds first guesses for the Gaussian and baseline parameters,
C        3) calls ITER8 (which calls MRQMIN repetitively to improve A, 
C             minimizing chisq, printing chisq and A at each iteration 
C             until convergence, and making a final MRQMIN call to 
C             retrieve the covariance matrix),
C        4) finds the relative uncertainties in A,
C        5) finds the Gaussian area and its relative uncertainty, and
C        6) prints a table of results for each peak and the baseline.
C     ITER8 prints intermediate values of A in "iter.txt".
C     MAINIO prints final values of A and peak areas and uncertianities
C        in "fitted.txt".
C     MRQMIN (Marquardt minimization) calls MRQCOF,GAUSSJ, and COVSRT.
C     MRQCOF calls fg4nb2, which calculates the fitting function and 
C        its explicit partial derivatives wrt A(1:ma).
C=========================TMH 10/22/2007===============================

        parameter (npksmax=7)
        character(len=40) dfile
        integer npks,npts,j0(npksmax+1)
        real amin(4*npksmax+2),amax(4*npksmax+2)
        open (1,file='fcond.txt',status='old',action='read')
        read(1,*)dfile
c        print*,dfile
        read(1,'(2x,i1,2x,i3)')npks,npts
c        print*,npks,npts
        read(1,*)j0(1:npks+1)
c        print*,j0(1:npks+1)
        ma=4*npks+2
        do 10,m=1,ma
           read(1,*)amin(m),amax(m)
c           print*,amin(m),amax(m)
10      continue
       
        call mainio(dfile,npts,npks,j0,ma,amin,amax)
        end program

c=====================================================================
        subroutine mainio(dfile,npts,npks,j0,ma,amin,amax)
        PARAMETER(pi=3.141592654)
        DIMENSION X(NPTS),Y(NPTS),SIG(NPTS),A(MA),LISTA(MA),amin(ma),
     *      amax(ma),COVAR(MA,MA),ALPHA(MA,MA),GUES(MA),ybl(npts),
     *      runc(ma),ynet(npts)
        integer guespkj,jlhm,jrhm,j0(npks+1)
        integer :: c,cr,cm
        real chisq,yf,pkarea,garea,amp,xpk,tt0,tt1
        character(len=40)dfile
        sqrtpid2=sqrt(pi)/2.0
c        print*,dfile
c        stop

c    Open data file, read x, y
       open (2,file=dfile,status='old',action='read')
       read(2,*) (x(j),y(j),j=1,npts)
       close (2)

c    Guess initial values GUES(1:ma)
c    Initial baseline slope and offset
       gues(ma-1)=(y(npts)-y(1))/(x(npts)-x(1))
       gues(ma)=y(1)
c    Subtract initial baseline
       do 5 j=1,npts
          ybl(j)=gues(ma-1)*(x(j)-x(1))+gues(ma)
5      continue
       do 6 j=1,npts
         ynet(j)=y(j)-ybl(j)
6      continue

c    Parameters for each peak in group
       do 11 m=0,npks-1
c    Peak amplitude, center
       j1=j0(m+1)
       j2=j0(m+2)
       amp=-1E20
       do 8 j=j1,j2
          if (ynet(j).gt.amp) then
            amp=ynet(j)
            xpk=x(j)
            guespkj=j
          end if
8      continue
       gues(4*m+1)=amp
       gues(4*m+2)=xpk
c    Locate left half maximum, starting from max
       do 9 j=guespkj,1,-1
         if (ynet(j).le.(gues(4*m+1)/2.0)) exit
9      continue
       jlhm=max(j,j1+1)
c    Locate right half maximum
       do 10 j=guespkj,npts
         if (ynet(j).le.(gues(4*m+1)/2.0)) exit
10     continue
       jrhm=min(j,j2-1)
c    Left half maximum; distortion = rightwidth/leftwidth
       gues(4*m+3)=gues(4*m+2)-x(jlhm)
       gues(4*m+4)=(x(jrhm)-gues(4*m+2))/gues(4*m+3)
11     continue
c    Print initial guesses
       print*,'gues',gues
       a(1:ma)=gues(1:ma)

c    Set up SIG (for the future deviations between y and yf)
       do 12, j=1,npts
          sig(j)=1.0
12     continue
       chisq=npts*1.0
       pause

c       call system_clock(c,cr,cm)
c       write(*,*) c ! writes current count
c       write(*,*) cr ! writes count rate
c       write(*,*) cm ! writes maximum count possible
c       write(*,*) real(c)/real(cr),' = current count in seconds'
c       tt0=real(c)/real(cr)

       call iter8(X,Y,SIG,NPTS,A,MA,amin,amax,COVAR,CHISQ,niter)

c       call system_clock(c,cr,cm)
c       write(*,*) c ! writes current count
c       write(*,*) cr ! writes count rate
c       write(*,*) cm ! writes maximum count possible
c       write(*,*) real(c)/real(cr),' = current count in seconds'
c       tt1=real(c)/real(cr)
c       print*,tt0,tt1,tt1-tt0, ' seconds'

      do 25 i=1,ma
         runc(i)=abs(sqrt(covar(i,i))/a(i))
25    continue

      open (4,file='fitted.txt',status='unknown',action='write')

      write(*,*)' '
      write(*,*)'pk#    height     center      width        distort
     * area'
      write(4,*)'MRQMIN iterated ',niter, 'times, gave final results:'
      write(4,*)'pk#    height     center      width        distort
     * area'
      do 35 m=1,ma-5,4
        ipk=(m+3)/4
        arealeft=sqrtpid2*a(m)*a(m+2)
        arearight=sqrtpid2*a(m)*a(m+2)*a(m+3)
        garea=arealeft+arearight
        uncleft=sqrt(runc(m)**2+runc(m+2)**2)*arealeft
        uncright=sqrt(runc(m)**2+runc(m+2)**2+runc(m+3)**2)*arearight
        runcarea=sqrt(uncleft**2+uncright**2)/garea
             
        write(*,'(I2,2X,5(F10.3,2X))') ipk,a(m:m+3),garea
        write(*,'(1X,A,3X,5(E10.2,2X))') 'ru', runc(m:m+3),runcarea
        write(4,'(I2,2X,5(F10.3,2X))') ipk,a(m:m+3),garea
        write(4,'(1X,A,3X,5(E10.2,2X))') 'ru', runc(m:m+3),runcarea
c            print*,a(i+(npks-1)),runc(i+(npks-1))
35      continue
        write(*,*)'baseline slope   offset'
        write(*,'(5X,2(F10.3,2X))')a(ma-1),a(ma)
        write(*,'(1X,A,3X,2(E10.2,2X))')'ru',runc(ma-1),runc(ma)
        write(4,*)'baseline slope   offset'
        write(4,'(5X,2(F10.3,2X))')a(ma-1),a(ma)
        write(4,'(1X,A,3X,2(E10.2,2X))')'ru',runc(ma-1),runc(ma)

c    Check area result by integration
        pkarea=0.0
        do 40 j=2,npts-1
         if (x(j).le.a(2)) then
           yf=a(1)*exp(-((x(j)-a(2))/a(3))**2)
         else
           yf=a(1)*exp(-((x(j)-a(2))/(a(3)*a(4)))**2)
         end if
         pkarea=pkarea+yf*(x(j+1)-x(j-1))/2
40      continue
        write(*,*) 'Area by summation = ',pkarea
        END SUBROUTINE

c==================================================================
      SUBROUTINE iter8(X,Y,SIG,NPTS,A,MA,amin,amax,COVAR,CHISQ,niter)
c  This subroutine runs MRQMIN iteratively until convergence of CHISQ,
c       printing A(1:ma) at each iteration, and then calls MRQMIN 
c       with ALAMDA=0.0 to retrieve the covariance matrix.
      PARAMETER (MMAX=20)
      DIMENSION X(npts),Y(npts),SIG(npts),A(MA),LISTA(MA),
     *  amin(ma),amax(ma),COVAR(ma,ma),ALPHA(ma,ma)

        MFIT=MA
        DO 14 I=1,MFIT
                LISTA(I)=I
14      CONTINUE
        ALAMDA=-1.
c        print*,'in main before first mrqmin call'
        CALL MRQMIN(X,Y,SIG,NPTS,A,MA,LISTA,MFIT,amin,amax,COVAR,ALPHA,
     *                  MA,CHISQ,ALAMDA)

        K=1
        ITST=0
        open (3,file='iter.txt',status='unknown',action='write')
c        WRITE(*,'(1X,T8,A,T20,A,T32,A,T44,A,T56,A,T68,A)') 'A(1)',
c     *          'A(2)','A(3)','A(4)','A(5)','A(6)'
        WRITE(3,'(1X,T8,A,T20,A,T32,A,T44,A,T56,A,T68,A)') 'A(1)',
     *          'A(2)','A(3)','A(4)','A(5)','A(6)'
c   Iteration loop returns to here
1       continue
        WRITE(*,'(/1X,A,I3,T18,A,F14.0,T47,A,E9.2)') 'Iteration #',K,
     *          'Chi-squared:',CHISQ,'ALAMDA:',ALAMDA
        WRITE(*,'(1X,6F12.4)') (A(I),I=1,ma)
        WRITE(3,'(/1X,A,I3,T18,A,F14.0,T47,A,E9.2)') 'Iteration #',K,
     *          'Chi-squared:',CHISQ,'ALAMDA:',ALAMDA
        WRITE(3,'(1X,6F12.4)') (A(I),I=1,ma)

c        print*,K,'in main before kth mrqmin call'
        K=K+1
        OCHISQ=CHISQ
        CALL MRQMIN(X,Y,SIG,NPTS,A,MA,LISTA,MFIT,amin,amax,COVAR,ALPHA,
     *                  MA,CHISQ,ALAMDA)  
c        print*,'chisq,ochisq in splitgfitn',chisq,ochisq
        IF (CHISQ.GT.OCHISQ) THEN
                ITST=0
        ELSE IF (ABS(OCHISQ-CHISQ).LT.0.1) THEN
C        ELSE IF (ABS(OCHISQ-CHISQ).LT.0.000001) THEN
                ITST=ITST+1
        ENDIF
        IF (ITST.LT.2) THEN
                GOTO 1
        ENDIF

c    Final call to MRQMIN with ALAMDA=0, to retrieve covariance matrix
        ALAMDA=0.0
        CALL MRQMIN(X,Y,SIG,NPTS,A,MA,LISTA,MFIT,amin,amax,COVAR,ALPHA,
     *                  MA,CHISQ,ALAMDA)
c        stop
      niter=k-1
      END SUBROUTINE

c====================================================================
      SUBROUTINE MRQMIN(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,amin,amax,
     *    COVAR,ALPHA,NCA,CHISQ,ALAMDA)
      PARAMETER (MMAX=20)
      DIMENSION X(NDATA),Y(NDATA),SIG(NDATA),A(MA),LISTA(MA),
     *  amin(ma),amax(ma),
     *  COVAR(NCA,NCA),ALPHA(NCA,NCA),ATRY(MMAX),BETA(MMAX),DA(MMAX)

c  occasional misbehavior of (alamda.lt.0) switch when alamda=0.0
      IF(ALAMDA.LT.0.)THEN
c       IF(ALAMDA.LT.(-1.0e-40))THEN
        KK=MFIT+1
        DO 212 J=1,MA
          IHIT=0
          DO 211 K=1,MFIT
            IF(LISTA(K).EQ.J)IHIT=IHIT+1
211        CONTINUE
          IF (IHIT.EQ.0) THEN
            LISTA(KK)=J
            KK=KK+1
          ELSE IF (IHIT.GT.1) THEN
            PAUSE 'Improper permutation in LISTA'
          ENDIF
212      CONTINUE
        IF (KK.NE.(MA+1)) PAUSE 'Improper permutation in LISTA'
        ALAMDA=0.001
        CALL MRQCOF(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,ALPHA,BETA,NCA,CHISQ)
         OCHISQ=CHISQ
        DO 213 J=1,MA
          ATRY(J)=A(J)
213      CONTINUE
      ENDIF
      DO 215 J=1,MFIT
        DO 214 K=1,MFIT
          COVAR(J,K)=ALPHA(J,K)
214      CONTINUE
        COVAR(J,J)=ALPHA(J,J)*(1.+ALAMDA)
        DA(J)=BETA(J)
215    CONTINUE
      CALL GAUSSJ(COVAR,MFIT,NCA,DA,1,1)
      IF(ALAMDA.EQ.0.)THEN
        CALL COVSRT(COVAR,NCA,MA,LISTA,MFIT)
        RETURN
      ENDIF

      DO 216 J=1,MFIT
c   Prevent each parameter from stepping outside its range [amin, amax]
        ATRY(LISTA(J))=max(amin(j),A(LISTA(J))+DA(J))
        atry(lista(j))=min(amax(j),atry(lista(j)))
216    CONTINUE
c      print*,'in mrqmin before second mrqcof call'
      CALL MRQCOF(X,Y,SIG,NDATA,ATRY,MA,LISTA,MFIT,COVAR,
     *    DA,NCA,CHISQ)

c      print*,'chisq,ochisq in mrqmin',chisq,ochisq
      IF(CHISQ.LT.OCHISQ)THEN
        ALAMDA=0.1*ALAMDA
        OCHISQ=CHISQ
        DO 218 J=1,MFIT
          DO 217 K=1,MFIT
            ALPHA(J,K)=COVAR(J,K)
217        CONTINUE
          BETA(J)=DA(J)
          A(LISTA(J))=ATRY(LISTA(J))
218      CONTINUE
      ELSE
        ALAMDA=10.*ALAMDA
        CHISQ=OCHISQ
      ENDIF
      RETURN
      END SUBROUTINE


c====================================================================
      SUBROUTINE GAUSSJ(A,N,NP,B,M,MP)
c  Gauss-Jordan elimination, solves set of ma equations for gradient
c       of CHISQ and step size of search
      PARAMETER (NMAX=50)
      DIMENSION A(NP,NP),B(NP,MP),IPIV(NMAX),INDXR(NMAX),INDXC(NMAX)
      DO 311 J=1,N
        IPIV(J)=0
311    CONTINUE
      DO 322 I=1,N
        BIG=0.
        DO 313 J=1,N
          IF(IPIV(J).NE.1)THEN
            DO 312 K=1,N
              IF (IPIV(K).EQ.0) THEN
                IF (ABS(A(J,K)).GE.BIG)THEN
                  BIG=ABS(A(J,K))
                  IROW=J
                  ICOL=K
                ENDIF
              ELSE IF (IPIV(K).GT.1) THEN
                PAUSE 'Singular matrix after GAUSSJ L11'
              ENDIF
312          CONTINUE
          ENDIF
313      CONTINUE
        IPIV(ICOL)=IPIV(ICOL)+1
        IF (IROW.NE.ICOL) THEN
          DO 314 L=1,N
            DUM=A(IROW,L)
            A(IROW,L)=A(ICOL,L)
            A(ICOL,L)=DUM
314        CONTINUE
          DO 315 L=1,M
            DUM=B(IROW,L)
            B(IROW,L)=B(ICOL,L)
            B(ICOL,L)=DUM
315        CONTINUE
        ENDIF
        INDXR(I)=IROW
        INDXC(I)=ICOL
        IF (A(ICOL,ICOL).EQ.0.) PAUSE 'Singular matrix. after GAUSSJ 15'
        PIVINV=1./A(ICOL,ICOL)
        A(ICOL,ICOL)=1.
        DO 316 L=1,N
          A(ICOL,L)=A(ICOL,L)*PIVINV
316      CONTINUE
        DO 317 L=1,M
          B(ICOL,L)=B(ICOL,L)*PIVINV
317      CONTINUE
        DO 321 LL=1,N
          IF(LL.NE.ICOL)THEN
            DUM=A(LL,ICOL)
            A(LL,ICOL)=0.
            DO 318 L=1,N
              A(LL,L)=A(LL,L)-A(ICOL,L)*DUM
318          CONTINUE
            DO 319 L=1,M
              B(LL,L)=B(LL,L)-B(ICOL,L)*DUM
319          CONTINUE
          ENDIF
321      CONTINUE
322    CONTINUE
      DO 324 L=N,1,-1
        IF(INDXR(L).NE.INDXC(L))THEN
          DO 323 K=1,N
            DUM=A(K,INDXR(L))
            A(K,INDXR(L))=A(K,INDXC(L))
            A(K,INDXC(L))=DUM
323        CONTINUE
        ENDIF
324    CONTINUE
      RETURN
      END SUBROUTINE

c========================================================
      SUBROUTINE COVSRT(COVAR,NCVM,MA,LISTA,MFIT)
c  Sorts covariance matrix.
      DIMENSION COVAR(NCVM,NCVM),LISTA(MFIT)
      DO 412 J=1,MA-1
        DO 411 I=J+1,MA
          COVAR(I,J)=0.
411      CONTINUE
412    CONTINUE
      DO 414 I=1,MFIT-1
        DO 413 J=I+1,MFIT
          IF(LISTA(J).GT.LISTA(I)) THEN
            COVAR(LISTA(J),LISTA(I))=COVAR(I,J)
          ELSE
            COVAR(LISTA(I),LISTA(J))=COVAR(I,J)
          ENDIF
413      CONTINUE
414    CONTINUE
      SWAP=COVAR(1,1)
      DO 415 J=1,MA
        COVAR(1,J)=COVAR(J,J)
        COVAR(J,J)=0.
415    CONTINUE
      COVAR(LISTA(1),LISTA(1))=SWAP
      DO 416 J=2,MFIT
        COVAR(LISTA(J),LISTA(J))=COVAR(1,J)
416    CONTINUE
      DO 418 J=2,MA
        DO 417 I=1,J-1
          COVAR(I,J)=COVAR(J,I)
417      CONTINUE
418    CONTINUE
      RETURN
      END SUBROUTINE

c=====================================================================
      SUBROUTINE MRQCOF(X,Y,SIG,NDATA,A,MA,LISTA,MFIT,ALPHA,BETA,NALP,
     *   CHISQ)
c  Find Marquardt coefficients, using x,y data and the fitting function
c       and its explicit partial derivatives wrt A(1:ma), found in
c       subroutine fg4nb2
      parameter(npksmax=7)
      DIMENSION X(NDATA),Y(NDATA),SIG(NDATA),ALPHA(NALP,NALP),BETA(MA),
     *   dyda(ma),LISTA(MFIT),A(MA)
      real x1
      integer npks,L(npksmax)
      npks=(ma-2)/4
c         L(n)=0 on left half of peak, L(n)=1 on right half

      x1=x(1)
      DO 509 J=1,MFIT
        DO 510 K=1,J
          ALPHA(J,K)=0.
510      CONTINUE
        BETA(J)=0.
509     CONTINUE
      L=0
      CHISQ=0.
      DO 515 I=1,NDATA
        do 512, n=1,npks
          if (x(i).lt.a(2+4*(n-1))) then
            L(n)=0
          else
            L(n)=1
          end if
512      continue
c        print*,i,'in mrqcof before funcs call',i,x(i),sig(i),L,dyda
c        pause
        CALL fg4nb2(X(I),A,YMOD,DYDA,MA,L,x1)
c        print*,i,'in mrqcof after funcs call',sig(i)
        SIG2I=1./(SIG(I)*SIG(I))
        DY=Y(I)-YMOD
        DO 514 J=1,MFIT
          WT=DYDA(LISTA(J))*SIG2I
c          wt=1.0
          DO 513 K=1,J
            ALPHA(J,K)=ALPHA(J,K)+WT*DYDA(LISTA(K))
513        CONTINUE
          BETA(J)=BETA(J)+DY*WT
514      CONTINUE
        CHISQ=CHISQ+DY*DY*SIG2I
515    CONTINUE
c      print*,'chisq',chisq 

      DO 517 J=2,MFIT
        DO 516 K=1,J-1
          ALPHA(K,J)=ALPHA(J,K)
516      CONTINUE
517    CONTINUE
c      print*,'end of mrqcof run'
      RETURN
      END SUBROUTINE


c====================================================================
      SUBROUTINE fg4nb2(XI,A,YMODI,DYDAI,NA,L,x1)
c  based on FGAUSS in Numerical Recipes 14.4, Press et al. 1986
c  modified for 4-parameter Gaussian + linear baseline.
c  Differences from FGAUSS: different "width", 0.5 inserted in EX,
c       *2.0 removed from FAC. "xgfit.doc" derives explicit formulas.
c  This routine is called by MRQCOF at each data point, each iteration.
c       dyda(1:ma) = explicit partial derivatives of yfit wrt a(1:ma),
c         used by MRQCOF to find steepest descent toward minimum chisq.
c   A(m)=amplitude, A(m+1)=center, A(m+2)=leftwidth, 
c      A(m+3)=rtwidth/leftwidth, 
c      A(ma-1)=baseline slope, A(ma)=baseline offset
c   L=0 left of pk center, =1 right of center
c   x1=x(1), needed for baseline expression

      parameter(npksmax=7)
      DIMENSION A(NA),DYDAI(NA)
      integer L(npksmax)
      real arg,ex,fac,ymodi

      YMODI=0.0
      do 610, i=1,na-5,4
        n=(i+3)/4
        if (L(n).eq.0) then
           ARG=(XI-A(i+1))/A(i+2)
        else
           ARG=(XI-A(i+1))/(A(i+2)*a(i+3))
        end if
        EX=EXP(-0.5*ARG**2)
        FAC=A(i)*EX*ARG
        YMODI=YMODI+A(i)*EX
        DYDAI(i)=EX
        DYDAI(i+1)=FAC/A(i+2)
        DYDAI(i+2)=FAC*ARG/A(i+2)
        if (L(n).eq.0) then
           DYDAI(i+3)=0.0
        else
           DYDAI(i+3)=FAC*ARG/A(i+3)
        end if
610   continue
      YMODI=YMODI+a(na-1)*(xi-x1)+a(na)
      DYDAI(na-1)=xi-x1
      DYDAI(na)=1.0
      RETURN
      END SUBROUTINE

c    Original code, NumRec FGAUSS.FOR
c      Y=0.
c      DO 11 I=1,NA-1,3
c        ARG=(X-A(I+1))/A(I+2)
c        EX=EXP(-ARG**2)       note missing factor 0.5, add it!
c        FAC=A(I)*EX*2.*ARG
c        Y=Y+A(I)*EX
c        DYDA(I)=EX
c        DYDA(I+1)=FAC/A(I+2)
c        DYDA(I+2)=FAC*ARG/A(I+2)
c11    CONTINUE
c      RETURN
c      END

