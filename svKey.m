function svKey(key)
global lepeak;
global legram;
global lechange;

if key==28
    lepeak=lepeak-1;
    lechange=1;
elseif key==29
    lepeak=lepeak+1;
    lechange=1;
elseif key==30
    legram=legram+1;
    lechange=1;
elseif key==31
    legram=legram-1;
    lechange=1;
elseif key==113
    lechange=-1;
elseif key==81
    lechange=-1;
else
    disp(num2str(key));
end

