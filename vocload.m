function [header, data] = vocload(file)
% vocload - imports VOCTEC chromatogram data files
%
% SYNOPSIS:     [header, data] = vocload(file)
%
% INPUT:        file - string represetation of the file name and extension
%
% OUTPUT:       header - array containing header information
%               data - array containing data information
%
% NOTES:        this mfile replaces the previous version as of 7/21/03 due to compiler issues with 'feval'
%

fid = fopen(file, 'r');
if (fid == -1)
    warnHandle = warndlg('Cannot open file for reading.','VOC Technologies');
end

header=[];
data=[];



% OLD HEADER CODE
% headLines=25;
%read the headers
% i=1;
% while  i<=headLines;
    %     tline = char(fgetl(fid));
    %     if i==2
    %         if isempty(tline)
    %             headLines=42;
    %         end
    %     end
    %     if ~ischar(tline), break, end
    %     header = strvcat(header, tline);
    %     i=i+1;
    % end
    str1='b';
    dd1=1;
while ~(strcmp(cell2mat(regexp(str1,'(\s*\d[.,]*)*','match')),str1) & ~strcmp(cell2mat(regexp(str1,'\s*','match')),str1)) & dd1>=0
    dd1=fgetl(fid);
    str1= char(dd1);
    if ~(strcmp(cell2mat(regexp(str1,'(\s*\d[.,]*)*','match')),str1) & ~strcmp(cell2mat(regexp(str1,'\s*','match')),str1))
        header = strvcat(header, str1);
    end
end
    
%check how many columns of data exist
% tline = fgetl(fid);
if isnumeric(str1) | dd1==-1
    warning(['Empty data file at: ' file]);
    data=[];
else
    pack;
    data=vcData(fid,str1);
end

function data=vcData(fid,str1)
    [data, ncols, errmsg, nxtindex] = sscanf(str1, '%f');

    %read the rest of the data and reshape
    data = [data;fscanf(fid,'%f')];
    fclose(fid);
    if abs(length(data)/ncols-round(length(data)/ncols))>0
        data(end)=[];
    end

    data = reshape(data, ncols, length(data)/ncols)';
