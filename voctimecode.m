function time = voctimecode(file)

% voctimecode - interprets filename code for VOCTEC chromatogram data files
%
% SYNOPSIS: time = voctimecode(file)
%
% INPUT:    file - string represetation of the file name and extension    
%
% OUTPUT:   filecode - structure of string arrays containing decoded filename info
%
% NOTES:    Here is an example of a filename that has been decoded
%
%           time = voctimecode('GO709T23.ASC')
%           
%           time = '19:23'


% check number and type of arguments
if nargin < 1
  error('Function requires one input argument');
elseif ~isstr(file)
  error('Input must be a string representing a filename');
elseif   ~isletter(file(5))
  disp('Filename is not in valid VOCTEC timecode format');
  time='00:00';
  return
end

% extract the portion that contains the time info
hour = upper(file(5));
minute = file(6:7);

% decode the time and change to military time
if hour == 'A';
    hour  = '00:';
elseif hour == 'B';
    hour = '01:';
elseif hour == 'C';
    hour = '02:';
elseif hour == 'D';
    hour = '03:';
elseif hour == 'E';
    hour = '04:';
elseif hour == 'F';
    hour = '05:';
elseif hour == 'G';
    hour = '06:';
elseif hour == 'H';
    hour = '07:';
elseif hour == 'I';
    hour = '08:';
elseif hour == 'J';
    hour = '09:';
elseif hour == 'K';
    hour = '10:';
elseif hour == 'L';
    hour = '11:';
elseif hour == 'M';
    hour = '12:';
elseif hour == 'N';
    hour = '13:';
elseif hour == 'O';
    hour = '14:';
elseif hour == 'P';
    hour = '15:';
elseif hour == 'Q';
    hour = '16:';
elseif hour == 'R';
    hour = '17:';
elseif hour == 'S';
    hour = '18:';
elseif hour == 'T';
    hour = '19:';
elseif hour == 'U';
    hour = '20:';
elseif hour == 'V';
    hour = '21:';
elseif hour == 'W';
    hour  = '22:';
elseif hour == 'X';
    hour = '23:';
end

% return the decoded time as a string
time = strcat(hour,minute);