function slideviewer
global lepeak;
global legram;
global lechange;
warning off MATLAB:divideByZero
lepeak=1;
legram=1;
lechange=1;
disp('Now Loading Data...');
uiload;
close all;
figure;
set(gcf,'KeyPressFcn',['svKey(((get(gcf,''CurrentCharacter'')+1)/2-.5)*2);']);

size(peakZ)
while lechange>-1
    if lechange==1
        disp('Goin');
        lepeak=mod(lepeak-1,length(peakZ)-1)+1;
        legram=mod(legram-1,length(peakZ(1).fileZ))+1;
        j=legram;
        k=lepeak+1;
        cPeak=peakZ(k).fileZ(j);
        x1=min(cPeak.pkbnd);
        x2=max(cPeak.pkbnd);
        ss1=find(abs(peakZ(1).fileZ(j).x-x1)==min(abs(peakZ(1).fileZ(j).x-x1)));
        ss2=find(abs(peakZ(1).fileZ(j).x-x2)==min(abs(peakZ(1).fileZ(j).x-x2)));
        ss1=ss1(1);
        ss2=ss2(1);
        xdata=peakZ(1).fileZ(j).x(ss1:ss2);
        ydata=peakZ(1).fileZ(j).y(ss1:ss2);
        gUnit=peakZ(k).fileZ(j).gParams;
        gdata=gaussfun(gUnit,xdata);
        sldata=gaussfun([0 0 0 gUnit(4) gUnit(5)],xdata);
        hold off;
        plot(xdata,ydata,'r+');
        hold on;
        plot(xdata,gdata,'b-');
        plot(xdata,sldata,'g.');
        fname=[str2mat(peakZ(1).fileZ(j).name) '-' num2str((x1+x2)/2)];
        title([fname  '.pks']);
        lechange=0;
    end
    pause(.2);
end
close all;
function f=gaussfun(A,x)
f = A(1)*exp(-0.5.*((x-A(2))/A(3)).^2)+A(4)*x+A(5);