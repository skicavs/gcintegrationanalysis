function slidetools
global MasterPeak;
global SLIDEVERSION;
global linehandles;
global manu;
global help;
global lstpeak;
global baroJump;
%#function dealKey
lstpeak=1;
baroJump=0;
% 1 is a placeholder year, month, day
SLIDEVERSION=1060406;
help=0;
warning off MATLAB:divideByZero
loadMaster;
norres=menu('How to Begin','New Integration','Resume an Old Integration');
if norres==1
    % show list of available files, return the selected ones
    selectedfiles = vocgetfiles;
    % setup the columns

    %tcol=menu('What Column is Time?','Column 1','Column 2','No Time Column');
    dcol=menu('Where are the data?','Column 1','Column 2','Column 3','Column 4');
    % load the data
    peakZ={};
    L=1;
    while L<=length(selectedfiles)
        % prep the data before choosing the peaks
        try
            [data, hdrinfo(L),time] = vocdataprep(selectedfiles(L),0);

            %[header, data] = vocload(char(selectedfiles(L)));
            %hdrinfo(L) = vocheadersProto(header);
            if size(data,1)>50 & size(data,2)>0
                peakZ(1).fileZ(L).name=selectedfiles(L);
                
                peakZ(1).fileZ(L).date=hdrinfo(L).date;
                %if tcol==3
                % generate a x column of just datapoints
                %    peakZ(1).fileZ(L).x=[1:length(data(:,dcol))];
                %else
                % peakZ(1).fileZ(L).x=data(:,tcol);
                % end
                peakZ(1).fileZ(L).x=time;
                peakZ(1).fileZ(L).y=data(:,dcol);
                peakZ(1).fileZ(L).area=0;
                peakZ(1).fileZ(L).pkbnd=[];
                peakZ(1).fileZ(L).gParam=[];
                peakZ(1).peakRoot=0;
                peakZ(1).peakStr=0;
                peakZ(1).datatype=0;
                peakZ(1).peakGroup=-1;
                peakZ(1).pkName='';
                peakZ(1).peakId={};
                peakZ(1).peakMisc.fitFun='@gaussfun';
                peakZ(1).peakMisc.peakFollow=''; % peakName to follow

                peakZ(1).plotsep=sum(data(:,1))/length(data(:,1))/7;
                peakZ(1).refData=[];
                L=L+1;
            else % data is TOo Small
                disp(['Data file ' cell2mat(selectedfiles(L)) ' is not valid or too small']);
                selectedfiles(L)=[];
                hdrinfo(L)=[];
            end
        catch
            disp(['Data file ' cell2mat(selectedfiles(L)) ' is not even remotely valid']);
            selectedfiles(L)=[];

        end
    end




    % average the chosen data
    % curavg = curSum./length(selectedfiles);

    % plot the data
%    astart = findstr(hdrinfo(1).columns,'A->');
%    bstart = findstr(hdrinfo(1).columns,'B->');
 %   columnA = deblank(hdrinfo(1).columns(astart:bstart-1));



    % begin peak selection part of analysis
end
if norres==2
    disp('Now loading data file ...');
    uiload;
    if exist('SLIDEVERSION')
        if SLIDEVERSION<1050712
            uiwait(msgbox('This data file you selected to load is not very compatible with the current version. No conversion can take place using the resume integration method. If you want to recover this data. Create a new integration with the same datafiles and load this file as a peaklist','Fatal Error','modal'));
            return
        elseif SLIDEVERSION<1050821
            oldData=1;
        else
            oldData=0;
            'okey dokey'
        end
    else
        uiwait(msgbox('This data file you selected to load is not very compatible with the current version. No conversion can take place using the resume integration method. If you want to recover this data. Create a new integration with the same datafiles and load this file as a peaklist','Fatal Error','modal'));
        return
    end
    peakZ=sortpeaks(peakZ);
    % getpklist = menu('Analysis Menu','Use only existing peaks','Load a peaklist into file','Back');
    % if getpklist==3
    %    getpklist==4;
    %end
    getpklist=1;

end
close;
% get ready for the slider
linehandles=slidestack(peakZ);
manu=0;
while manu>-1
    pause(.2);
    if manu==1.1 % load le list
        disp('Now loading peaklist file ...');
        [peaklist,pooknums,fver,vodka,isTool]=safeload;
        oldver=0;
        if isempty(fver)
            uiwait(msgbox('This data file you selected to load is not very compatible with the current version. A conversion will take place, but expect peaks to have moved.','Warning','modal'));
            oldver=1;
        end

        close;
        % get ready for the slider
        linehandles=slidestack(peakZ);
        n=1;
        nsel=1;

        openz=1;
        if menu('Turn off graphic output while running?','Yes','No')==1
            close all;
            openz=0;
        end
        for k=1:size(peaklist,1)
            pizeakFollow=cell2mat(isTool(k));
            if isempty(pizeakFollow) % integrate the leaders first
                peakZ(end+1)=peakZ(end);
                peakZ(end).fileZ(1).pkbnd=[peaklist(k,[1,2])];
                peakZ(end).peakRoot=[peaklist(k,[1,2])];
                peakZ(end).peakStr=peaklist(k,3);
                peakZ(end).peakId={};
                peakZ(end).pkName=str2mat(pooknums(k));
                peakZ(end).refData=cell2mat(vodka(k));
                peakZ(end).peakMisc.peakFollow=pizeakFollow;
                peakZ=slideSearch(peakZ,[peaklist(k,[1,2])],1);
                pause(.5);
                exportResults(peakZ,1);
            end
        end
        peakZ=sortpeaks(peakZ);
        for k=1:size(peaklist,1)
            pizeakFollow=cell2mat(isTool(k));
            if ~isempty(pizeakFollow) %is a tool
                peakZ(end+1)=peakZ(end);
                peakZ(end).fileZ(1).pkbnd=[peaklist(k,[1,2])];
                peakZ(end).peakRoot=[peaklist(k,[1,2])];
                peakZ(end).peakStr=peaklist(k,3);
                peakZ(end).peakId={};
                peakZ(end).pkName=str2mat(pooknums(k));
                peakZ(end).refData=cell2mat(vodka(k));
                peakZ(end).peakMisc.peakFollow=pizeakFollow;
                peakZ=slideSearch(peakZ,[peaklist(k,[1,2])],1);
                pause(.5);
                exportResults(peakZ,1);
            end
        end
        peakZ=sortpeaks(peakZ);
        if openz==0
            linehandles=slidestack(peakZ);
        end
        manu=0;
    end

    if manu==1
        [xd,yd,zd]=ginput(2);
        if prod(zd)==1
            [pkName,cPeak]=identifyPeak(xd);
            pkstr = menu('Search Range','    High (Low Peak Density, High Peak Strength)    ','   Medium(High Peak Density or Low Peak Strength)    ','     Low (High Peak Density and Low Peak Strength)     ','     SuperLow (High Peak Density and Low Peak Strength)     ','     Zero (Drifting Feature Disabled)     ');
            if pkstr==1
                pkstr=400;
            elseif pkstr==2
                pkstr=150;
            elseif pkstr==3
                pkstr=75;
            elseif pkstr==4
                pkstr=30;
            elseif pkstr==5
                pkstr=0;
            end
            grplst=grpList(peakZ);
            grp=menu('Peak Group',grplst)
            if grp==1
                grp=-1;
            else
                grp=grp-1;
            end
            peakslist={'None'};
            for k=2:length(peakZ)
                peakslist(k)={peakZ(k).pkName};
            end
            if length(peakZ)>1
                jpk=menu('Follow a specific Peak?',peakslist);
            else
                jpk=1;
            end

            peakZ=slider(peakZ,xd,lstpeak,pkstr,cPeak,peakZ(jpk).pkName,pkName);

            % peakZ(1)
            peakZ(end).peakGroup=grp;


            peakZ=sortpeaks(peakZ);
            exportResults(peakZ,1);
        end
        manu=0;
    end
    if manu==55
        peakZ=sortpeaks(peakZ);
        manu=0;
    end
    if manu==99
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k)={peakZ(k).pkName};
        end
        jpk=menu('Which Peak?',peakslist);
        if jpk>1
            hy=menu('Are you sure you want to delete peak?','Yes','No');
            if hy==1
                close;
                if jpk>1
                    peakZ(jpk)=[];
                end
                % redraw all data without the peak
                linehandles=slidestack(peakZ);
                % handles = makemenu(gcf, labels, calls);
            end
        end
        manu=0;
    end
    if manu==4422
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k)={peakZ(k).pkName};
        end
        jpk=menu('Which Peak?',peakslist);
        hbo=0;
        if ~isempty(peakZ(jpk).peakMisc.peakFollow)
            hbo=menu('Are you sure you wish to rerail a follower peak?','Yes','No');
            if hbo==2
                jpk=1;
            end

        end
        if jpk>1
            uiwait(msgbox('Select Starting Graph Using Arrow Keys'));
            speak=lstpeak;
            [xd yd]=ginput(2);
            x1=min(xd);
            x2=max(xd);
            if hbo==1
                peakZ(jpk).peakRoot=[x1 x2];
                for npn=[2:length(peakZ)]
                    if strcmp(peakZ(jpk).peakMisc.peakFollow,peakZ(npn).pkName);
                        peakZ(npn).peakRoot=peakZ(npn).fileZ(speak).pkbnd;
                    end
                end
            end
            uiwait(msgbox('Select Finishing Graph Using Arrow Keys'));
            epeak=lstpeak;
            pkstr = menu('Search Range','    High (Low Peak Density, High Peak Strength)    ','   Medium(High Peak Density or Low Peak Strength)    ','     Low (High Peak Density and Low Peak Strength)     ','     SuperLow (High Peak Density and Low Peak Strength)     ','     Zero (Drifting Feature Disabled)     ');
            if pkstr==1
                pkstr=400;
            elseif pkstr==2
                pkstr=150;
            elseif pkstr==3
                pkstr=75;
            elseif pkstr==4
                pkstr=30;
            elseif pkstr==5
                pkstr=0;
            end
            % uiwait(msgbox('Select Peak'));
            doRedraw=menu('Would you like to redraw the entire graph after rerailing is finish (Yes is recommended)?','Yes','No');

            % find what x-value the user probably clicked
            s1=find(abs(peakZ(1).fileZ(speak).x-x1)==min(abs(peakZ(1).fileZ(speak).x-x1)));
            s2=find(abs(peakZ(1).fileZ(speak).x-x2)==min(abs(peakZ(1).fileZ(speak).x-x2)));
            oleRef=peakZ(jpk).refData; % used if the peak doesnt include the last peak
            peakZ(jpk).refData=peakZ(1).fileZ(speak).y(s1:s2);
            peakZ(jpk).pkbnd=[x1,x2];
            oleStr=peakZ(jpk).peakStr;
            peakZ(jpk).peakStr=pkstr;
            xtemp=[x1 x2];
            hold on;
            qd=sign(epeak-speak);
            if qd==0
                qd=1;
            end

            for n=[speak:qd:epeak]
                [peakZ xtemp]=doPeak(peakZ,xtemp,jpk,n);
                nextPeak={peakZ(jpk).pkName};
                stillRecur=1;
                while length(nextPeak)>0
                    captPeak=cell2mat(nextPeak(end));
                    nextPeak(end)=[];
                    for npn=[2:length(peakZ)]
                        if strcmp(peakZ(npn).peakMisc.peakFollow,captPeak)
                            [peakZ bogus]=doPeak(peakZ,peakZ(npn).fileZ(n).pkbnd,npn,n);
                            nextPeak(end+1)={peakZ(npn).pkName};
                        end
                    end
                end

            end

            if epeak<length(peakZ)
                peakZ(jpk).peakStr=oleStr;
                peakZ(jpk).refData=oleRef;
            end
            if doRedraw==1
                hold off;
                linehandles=slidestack(peakZ);
            end
        end
        manu=0;
    end
    if manu==9989 % code to delete chromatograph
        flecnt=lstpeak;
        hy=menu('Are you sure you want to delete chromatogram?','Yes','No');
        if hy==1
            for abk=1:length(peakZ)
                peakZ(abk).fileZ(flecnt)=[]
            end
            if lstpeak>length(peakZ(1).fileZ)
                lstpeak=length(peakZ(1).fileZ);
            end
            linehandles=slidestack(peakZ);
            % handles = makemenu(gcf, labels, calls);
        end
        manu=0;

    end
    if manu==947
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k)={peakZ(k).pkName};
        end
        jpk=menu('Which Peak?',peakslist);
        if jpk>1
            uiwait(msgbox('Select Reference Graph Using Arrow Keys'));
            [xd yd]=ginput(2);
            flecnt=lstpeak;
            x1=xd(1);
            x2=xd(2);
            ss1=max(find(abs(peakZ(1).fileZ(flecnt).x-x1)==min(abs(peakZ(1).fileZ(flecnt).x-x1))));
            ss2=max(find(abs(peakZ(1).fileZ(flecnt).x-x2)==min(abs(peakZ(1).fileZ(flecnt).x-x2))));
            peakZ(jpk).fileZ(flecnt).pkbnd=[xd];
            peakZ(jpk).fileZ(flecnt).pkrange=[peakZ(1).fileZ(flecnt).x(ss1:ss2)];
            peakZ(jpk).fileZ(flecnt).area=pkInt(peakZ(1).fileZ(flecnt),[ss1:ss2]);
            plot(peakZ(1).fileZ(flecnt).x([ss1,ss2]),peakZ(1).fileZ(flecnt).x([ss1,ss2]),'kh');
        end
        manu=0;
    end
    if manu==77
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k)={[peakZ(k).pkName '  Group ' num2str(peakZ(k).peakGroup)]};
        end
        jpk=menu('Which Peak?',peakslist);
        grplst=grpList(peakZ);
        if jpk>1
            grp=menu('Peak Group',grplst)
            peakZ(jpk).peakGroup=grp-1;
        end
        manu=0;
    end
    if manu==6969
        keyboard;
        manu=0;
    end
    if manu==9 | manu==-1
        exportResults(peakZ,0);
        if manu>0
            manu=0;
        end
    end
end
function peakZ=groupInt(peakZ)
groupCount=1;
n=1;
while groupCount>0
    groupie=[peakZ.peakGroup];
    groupie=find(groupie==n);
    groupCount=length(groupie);
    for ikj=1:length(peakZ(1).fileZ)
        rbnd=[];
        lbnd=[];
        for ijk=1:length(groupie)
            cng=peakZ(groupie(ijk)).fileZ(ikj).pkrange;
            lbnd=[lbnd;min(cng)];
            rbnd=[rbnd;max(cng)];
        end
        rbnd=max(rbnd);
        lbnd=min(lbnd);
        % rbnd=[peakZ(1).fileZ(ikj).x(rbnd) peakZ(1).fileZ(ikj).y(rbnd)];
        % lbnd=[peakZ(1).fileZ(ikj).x(lbnd) peakZ(1).fileZ(ikj).y(lbnd)];
        if ~isempty(rbnd)
            x=peakZ(1).fileZ(ikj).x(lbnd:rbnd);
            y=peakZ(1).fileZ(ikj).y(lbnd:rbnd);
            m=(y(end)-y(1))/(x(end)-x(1));
            baseline=m*(x-x(1))+y(1);

            for ijk=1:length(groupie)
                cng=peakZ(groupie(ijk)).fileZ(ikj).pkrange;
                %  figure
                %  plot(baseline(cng-lbnd+1));
                %  hold on;
                %  plot(peakZ(1).fileZ(ikj).y(cng))
                %  pause(.2);
                %  close
                peakZ(groupie(ijk)).fileZ(ikj).area=rawInt(peakZ(1).fileZ(ikj).x(cng),peakZ(1).fileZ(ikj).y(cng),baseline(cng-lbnd+1));
            end
        end
    end
    n=n+1;
end

function grplst=grpList(peakZ)
grplst={'No Group'};
bozo=0;
grps=max([peakZ.peakGroup]);
for n=1:grps
    tempStr=['Group '  num2str(n) ': '];
    for tappy=[2:length(peakZ)]
        if peakZ(tappy).peakGroup==n
            tempStr=[tempStr peakZ(tappy).pkName ' '];
        end
    end
    grplst(n+1)={tempStr};
end
grplst(end+1)={['New Group']};


function [peaklist,pooknums,SLIDEVERSION,vodka,toolshed]=safeload
uiload;
str={peakZ(2:end).pkName};
[s,v] = listdlg('PromptString','Select Peaks to Load:','ListString',str);
if v==1
    peakZ=peakZ([1 s+1]);
end

peaklist=[];
pooknums={};
vodka={};
toolshed={};
try
    for k=2:length(peakZ)
        peaklist=[peaklist;[peakZ(k).fileZ(end).pkbnd peakZ(k).peakStr peakZ(k).peakGroup]];
        pooknums(end+1)={peakZ(k).pkName};
        if isempty(peakZ(k).refData)
            x1=min(peakZ(k).peakRoot);
            x2=max(peakZ(k).peakRoot);
            ss1=max(find(abs(peakZ(1).fileZ(lstpeak).x-x1)==min(abs(peakZ(1).fileZ(lstpeak).x-x1))));
            ss2=max(find(abs(peakZ(1).fileZ(lstpeak).x-x2)==min(abs(peakZ(1).fileZ(lstpeak).x-x2))));
            peakZ(k).refData=peakZ(1).fileZ(lstpeak).y(ss1:ss2);
        end
        vodka(end+1)={peakZ(k).refData};
        toolshed(end+1)={peakZ(k).peakMisc.peakFollow};
    end
catch
    uiwait(msgbox('File selected is not of compatible file-format with the current version of SlideTools'));
end



function [xd,flecnt,pkstr]=slidepick()
global lstpeak;
[xda yd]=ginput(2);
% order x-values
xd=[min(xda);max(xda)];
flecnt=lstpeak;



function linehandles=slidestack(peakZ)
n=1;
close all;
figure
hold on;
linehandles=[];
while n<=length(peakZ(1).fileZ)
    % alternate colors
    pz=mod(n,5);
    if pz==0
        cr='r-';
    end
    if pz==1
        cr='b-';
    end
    if pz==2
        cr='g-';
    end
    if pz==3
        cr='c-';
    end
    if pz==4
        cr='m-';
    end
    h=plot(peakZ(1).fileZ(n).x,peakZ(1).fileZ(n).y+n*peakZ(1).plotsep,cr);
    if length(peakZ)>1
        for k=2:length(peakZ)
            % pull in coordinates from the peakZ file
            x1=peakZ(k).fileZ(n).pkbnd(1);
            x2=peakZ(k).fileZ(n).pkbnd(2);
            ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
            ss2=max(find(abs(peakZ(1).fileZ(n).x-x2)==min(abs(peakZ(1).fileZ(n).x-x2))));
            plot(peakZ(1).fileZ(n).x(ss1),peakZ(1).fileZ(n).y(ss1)+n*peakZ(1).plotsep,'mo','MarkerSize',2);
            plot(peakZ(1).fileZ(n).x(ss2),peakZ(1).fileZ(n).y(ss2)+n*peakZ(1).plotsep,'m+','MarkerSize',2);
            if ~isempty(peakZ(k).fileZ(n).gParams) % plot gaussians with everything else
                slxdata=peakZ(1).fileZ(n).x(ss1:ss2);
                gUnit=peakZ(k).fileZ(n).gParams;
                gdata=gaussfun(gUnit,slxdata)+n*peakZ(1).plotsep;
                plot(slxdata,gdata,'k-')
            end
        end
    end
    linehandles=[linehandles;h];
    n=n+1;
end
title(peakZ(1).fileZ(1).name,'Interpreter','None');
set(gcf,'KeyPressFcn',['dealKey(((get(gcf,''CurrentCharacter'')+1)/2-.5)*2);']);
set(gcf,'DeleteFcn','global manu;manu=-1;')
labels = str2mat( ...
    '&Peak Menu', ...
    '>&Select Peak^D', ...
    '>&Delete Peak', ...
    '>&Change Peak Group', ...
    '>&Edit Specific Peak', ...
    '>De&lete Specific Chromatograph', ...
    '>Re-Rail Peak', ...
    '>Load Peaklist...', ...
    '>&Save Progress', ...
    '>&Exit' ...
    );
calls = str2mat( ...
    '', ...
    'global manu; manu=1;', ...
    'global manu; manu=99;', ...
    'global manu; manu=77;', ...
    'global manu; manu=947;', ...
    'global manu; manu=9989;', ...
    'global manu; manu=4422;', ...
    'global manu; manu=1.1;', ...
    'global manu; manu=9;', ...
    'global manu; manu=-1;' ...
    );
handles = makemenu(gcf, labels, calls);


function [peakZ]=sortpeaks(peakZ)
[null numpeaks] = size(peakZ); %find the number of peaks that have been selected

%for L=1 : numpeaks %get all the left sides of the root peaks in an array to sort
%    tosort(L) = sum(peakZ(L).peakRoot)/2;
   
%end
tosort={peakZ.pkName}
[leftpeak perm] = sort(tosort); %sort them
peakZ=peakZ(perm);
disp('Peak list sorted....')


function exportResults(peakZ,autom)
global lstpeak;
global SLIDEVERSION;
try
    if autom==1
        % save 'temp.mat';
        pathname=pwd;
        filename='tempFile.csv';
        filterindex=2;
        drm=',';
    else
        % uisave;
        [filename, pathname, filterindex] = uiputfile({'*.csv','Comma Seperated Values (*.csv)';'*.txt',  'Tab Delimited File (*.txt)'},'Save','integration.csv');
        if filterindex==2
            drm='\t';
        elseif filterindex==1
            drm=',';
        else
            return
        end
    end

    peakZ=groupInt(peakZ);

    %drm=',';
    saveDir=pwd;
    eval(['cd ' pathname]);
    eval(['save ' filename(1:end-4)  '.mat']);
    % pathname=[];
    % filename=input('Filename for area table ==>  ','s');

    % basically chad's output code
    % write a bunch of files, append if file exists
    % create the datatable with filenames, retention time, and corrosponding peak area
    disp('Now writing data file ...')


    fid1 = fopen([filename],'w');
    % must omit to allow unmix to read
    fprintf(fid1,['FILENAME' drm]);
    fprintf(fid1,['DAY OF WEEK' drm]);
    fprintf(fid1,['DATE/TIME' drm]);

    % write names for each top of each column
    for columnnames = 2:length(peakZ);
        cRoot=sum(peakZ(columnnames).peakRoot)/2;
        cN=zpad(cRoot,4);
        if ~isfield(peakZ(columnnames),'pkName')
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        if isempty(peakZ(columnnames).pkName)
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        fprintf(fid1,['%s' drm],peakZ(columnnames).pkName);
    end

    fprintf(fid1,'TOTAL\n');

    % write out the filenames as the leftmost column
    for j = 1:length(peakZ(1).fileZ);

        % titles of original chromatogram files(omit for unmix friendly version)
        % filter the names from the network cockygobble
        b=char(peakZ(1).fileZ(j).name);
        b=regexp(b,'.*?(?<filename>\w*[.]\w*)','names');
        b=b(end).filename;
        fprintf(fid1,['%s' drm],b);
        if SLIDEVERSION<1051019
            peakZ(1).fileZ(j).date=hdrinfo(j).date;
        end
        [dayoweek, dayt] = strtok(peakZ(1).fileZ(j).date,',');

        dateloc = findstr(dayt,'-');
        dayt = dayt(dateloc(1)-2:dateloc(end));
        time = voctimecode(char(peakZ(1).fileZ(j).name));
        dayt =  strcat(dayt,time);

        fprintf(fid1,['%s' drm],dayoweek);
        fprintf(fid1,['%s' drm], dayt);

        % write out the data into its correct location     
        pkArez=0;
        for k = 2:length(peakZ);
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).area);
            % calcuate the TOTAL column
            pkArez=pkArez+peakZ(k).fileZ(j).area;
        end
        fprintf(fid1,['%12.4f' drm],pkArez);
        fprintf(fid1,'\n');
    end
    fclose(fid1);
    disp('Now writing gauss file ...')
    fid1 = fopen([filename(1:end-4) 'g' filename(end-3:end)],'w');
    % must omit to allow unmix to read
    fprintf(fid1,['FILENAME' drm]);
    fprintf(fid1,['DAY OF WEEK' drm]);
    fprintf(fid1,['DATE/TIME' drm]);

    % write names for each top of each column
    for columnnames = 2:length(peakZ);
        cRoot=sum(peakZ(columnnames).peakRoot)/2;
        cN=zpad(cRoot,4);
        if ~isfield(peakZ(columnnames),'pkName')
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        if isempty(peakZ(columnnames).pkName)
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        fprintf(fid1,[peakZ(columnnames).pkName drm peakZ(columnnames).pkName '-H' drm peakZ(columnnames).pkName '-HW' drm peakZ(columnnames).pkName '-A' drm peakZ(columnnames).pkName  '-TA' drm peakZ(columnnames).pkName  '-S2N' drm]);
    end
    fprintf(fid1,'\n');
    % write out the filenames as the leftmost column
    for j = 1:length(peakZ(1).fileZ);

        % titles of original chromatogram files(omit for unmix friendly version)
        % filter the names from the network cockygobble
        b=char(peakZ(1).fileZ(j).name);
        b=regexp(b,'.*?(?<filename>\w*[.]\w*)','names');
        b=b(end).filename;
        fprintf(fid1,['%s' drm],b);
        if SLIDEVERSION<1051019
            peakZ(1).fileZ(j).date=hdrinfo(j).date;
        end
        [dayoweek, dayt] = strtok(peakZ(1).fileZ(j).date,',');

        dateloc = findstr(dayt,'-');
        dayt = dayt(dateloc(1)-2:dateloc(end));
        time = voctimecode(char(peakZ(1).fileZ(j).name));
        dayt =  strcat(dayt,time);

        fprintf(fid1,['%s' drm],dayoweek);
        fprintf(fid1,['%s' drm], dayt);

        % write out the data into its correct location
        pkArez=0;
        for k = 2:length(peakZ);
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(2));
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(1));
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(3));
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(3)*sqrt(2*pi)*peakZ(k).fileZ(j).gParams(1));
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).area);
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(6));
            % calcuate the TOTAL column
        end
        fprintf(fid1,'\n');
    end
    fclose(fid1);
    eval(['cd ' saveDir]);
catch
    warning('Error saving output files. No Output was saved');
end

function [peakZ]=slider(peakZ,xd,flecnt,pkstr,cPeak,peakFollowName,pkName)

jkt=peakZ(1).fileZ(1).x;
x1=xd(1);
x2=xd(2);
% find what x-value the user probably clicked
s1=find(abs(peakZ(1).fileZ(flecnt).x-x1)==min(abs(peakZ(1).fileZ(flecnt).x-x1)));
s2=find(abs(peakZ(1).fileZ(flecnt).x-x2)==min(abs(peakZ(1).fileZ(flecnt).x-x2)));
s3=s2-s1;
fcknug=[];
peakZ(end+1)=peakZ(end);
peakZ(end).fileZ(flecnt).pkbnd=[x1 x2];
peakZ(end).pkName=pkName;
peakZ(end).peakRoot=[x1 x2];
peakZ(end).peakStr=pkstr;
peakZ(end).peakId=cPeak;
peakZ(end).refData=[peakZ(1).fileZ(flecnt).y(s1:s2)];
peakZ(end).peakMisc.fitFun='@gaussfun';
peakZ(end).peakMisc.peakFollow=peakFollowName;
for npn=[2:length(peakZ)] % this sets to follower distance
    if strcmp(peakFollowName,peakZ(npn).pkName);
        peakZ(npn).peakRoot=peakZ(npn).fileZ(flecnt).pkbnd;
    end
end
peakZ=slideSearch(peakZ,xd,flecnt)



function [peakZ]=slideSearch(peakZ,xd,flecnt)

hold on;
xIn=xd;
for n=[flecnt+1:length(peakZ(1).fileZ)]
    [peakZ xtemp]=doPeak(peakZ,xIn,length(peakZ),n);
    xIn=xtemp;
end
xIn=xd;
for n=[flecnt:-1:1]
    [peakZ xtemp]=doPeak(peakZ,xIn,length(peakZ),n);
    xIn=xtemp;
end
hold off;


function [peakZ,xTemp]=doPeak(peakZ,xd,peakNum,n)
% parameters for Dr. O's AntiShifting
% BAROBARO
TOLSNR=80; % gaussing must be a at least 40 snr in order to even attempt to shift base
TOLHEIGHT=6; % 600% higher than the noise
TOLDRIFT=3; % 3 times larger than the peak half-width

% find the shift of the peaks
colorZ='k';
cPeak=peakZ(peakNum).peakId;
pkstr=peakZ(peakNum).peakStr;
s3=length(peakZ(peakNum).refData)-1;
peakZ(peakNum).fileZ(n).x=[];
peakZ(peakNum).fileZ(n).y=[];
dumAss=0;
disp(['Current file:' cell2mat(peakZ(1).fileZ(n).name) '; Peak: ' num2str(sum(peakZ(peakNum).peakRoot)/2) '.pks']);
peaky=peakZ(peakNum).peakMisc.peakFollow;
if ~isempty(peaky)
    cshft=0;
    cn=0;
    for k=[2:length(peakZ)]
        if strcmp(peakZ(k).pkName,peaky)
            px1=min(peakZ(k).peakRoot); % the seed x1 coordinate of the leader peak
            %px1=max(find(abs(peakZ(1).fileZ(n).x-px1)==min(abs(peakZ(1).fileZ(n).x-px1))));
            px2=min(peakZ(k).fileZ(n).pkbnd); % the current x1 coordinate of the leader peak
            %px2=max(find(abs(peakZ(1).fileZ(n).x-px2)==min(abs(peakZ(1).fileZ(n).x-px2))));
            pxx1=min(peakZ(peakNum).peakRoot); % the seed x1 coordinate of the follower peak
            %pxx1=max(find(abs(peakZ(1).fileZ(n).x-pxx1)==min(abs(peakZ(1).fileZ(n).x-pxx1))));
            % newX is the
            cshft=pxx1+(px2-px1); % starting retention time
            pxx1;
            iShift=px2-px1;
            k;
            % disp(['I am ' peaky ' and I am a ' peakZ(k).pkName]);
            % try another method where you just try to make sure that the
            % distance between the peaks is the same
            % px1=min(peakZ(k).peakRoot);
            % px2=max(peakZ(k).fileZ(n).pkbnd);
            % pxx1=min(peakZ(peakNum).peakRoot);
            % newX is the root location of x plus the change from the
            % leader graph
            % cshft=pxx1+(px2-px1);
            % iShift=px2-px1
            cn=cn+1;
        end
    end
    if cn>0
        % shft=cshft;
        x1=cshft;
        ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
        % ss1=cshft;
        % x1=peakZ(1).fileZ(n).x(ss1);
        ss2=ss1+s3;
        x2=peakZ(1).fileZ(n).x(ss2);
        ssnr=0.0;
        shft=0;
        cng=[ss1:ss2];
    else
        dumAss=1; % no leader found
        warning(['Leader peak ' peaky ' is AWOL']);
    end
end

if (isempty(peaky) | dumAss)
    x1=min(xd);
    x2=max(xd);


    % locate the x boundary coordinates (time or datapt#) in the current file
    ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
    ss2=max(find(abs(peakZ(1).fileZ(n).x-x2)==min(abs(peakZ(1).fileZ(n).x-x2))));

    while ss2~=ss1+s3
        while ss2<ss1+s3
            ss1=ss1-round(rand*2);
            ss2=ss2+round(rand*2);
        end
        while ss2>ss1+s3
            ss1=ss1+round(rand*2);
            ss2=ss2-round(rand*2);
        end
    end
    % create the initial search range
    cng=[ss1:ss2];
    x1=peakZ(1).fileZ(n).x(ss1);
    x2=peakZ(1).fileZ(n).x(ss2);
    % determine current graph's collision boundaries
    leftBound=min(peakZ(1).fileZ(n).x);
    rightBound=max(peakZ(1).fileZ(n).x);
    for bjt=[2:peakNum-1]
        dPeak=peakZ(bjt).fileZ(n).pkbnd;
        if (x1+x2)/2>sum(dPeak)/2 & ~strcmp(peakZ(bjt).peakMisc.peakFollow,peakZ(peakNum).pkName) % & isempty(peakZ(bjt).peakMisc.peakFollow) % only prevents collisions between leader peaks
            leftBound=max(dPeak);
        end
    end
    for bjt=[peakNum-1:-1:2]
        dPeak=peakZ(bjt).fileZ(n).pkbnd;
        if (x1+x2)/2<sum(dPeak)/2 & ~strcmp(peakZ(bjt).peakMisc.peakFollow,peakZ(peakNum).pkName) % & isempty(peakZ(bjt).peakMisc.peakFollow) % only prevents collisions between leader peaks
            rightBound=min(dPeak);
        end
    end
    leftBound=find(abs(peakZ(1).fileZ(n).x-leftBound)==min(abs(peakZ(1).fileZ(n).x-leftBound)));
    rightBound=find(abs(peakZ(1).fileZ(n).x-rightBound)==min(abs(peakZ(1).fileZ(n).x-rightBound)));
    [shft,boobless,ssnr]=jaque(peakZ(peakNum).refData,peakZ(1).fileZ(n).y,cng,pkstr,[leftBound rightBound]);
    if ssnr<10
        colorZ='b';
    end
    if ssnr<0
        colorZ='r';
    end
    if ssnr<0
        disp('Slide method yielded exceptionally poor results, disabling');
        shft=0;
    end
end

if length(peakZ(1).fileZ(n).x)<(ss1+s3+shft) % redonkulously small data set
    warning(['Seriousy short data in ' cell2mat(peakZ(1).fileZ(n).name)]);
    peakZ(peakNum).fileZ(n).pkbnd=[1 3];
    peakZ(peakNum).fileZ(n).pkrange=[1 2 3];
    peakZ(peakNum).fileZ(n).area=-1;
    peakZ(peakNum).fileZ(n).gParams=[0 1 1 0 0 0];
else % actually do stuff
    % plot the new peak boundaries
    c1=[peakZ(1).fileZ(n).x(ss1+shft)];
    c2=[n*peakZ(1).plotsep+peakZ(1).fileZ(n).y(ss1+shft)];
    plot(c1,c2,[colorZ 'o'],'MarkerSize',2)
    % junkX1=[junkX1 c1];
    % junkY1=[junkY1 c2];
    % plot the new peak boundaries
    %  c1=[peakZ(1).fileZ(n).x(ss2+shft)];
    %  c2=[n*peakZ(1).plotsep+peakZ(1).fileZ(n).y(ss2+shft)];

    %   plot(c1,c2,[colorZ '+'],'MarkerSize',2)
    %junkX2=[junkX2 c1];
    %junkY2=[junkY2 c2];
    % update the screen
    % pause(.1)
    % set x1 and x2 to the new values
    x1=peakZ(1).fileZ(n).x(ss1+shft);
    x2=peakZ(1).fileZ(n).x(ss2+shft);
    xTemp=[x1 x2];
    % store values and integrate
    peakZ(peakNum).fileZ(n).pkbnd=[x1 x2];
    peakZ(peakNum).fileZ(n).pkrange=[cng+shft];
    peakZ(peakNum).fileZ(n).area=pkInt(peakZ(1).fileZ(n),cng+shft);

    % gaussian addition

    xdata=peakZ(1).fileZ(n).x(ss1+shft:ss1+s3+shft);
    ydata=peakZ(1).fileZ(n).y(ss1+shft:ss1+s3+shft);

    %off scale points delete code
    ofScal=find(ydata>1e6);
    ydata(ofScal)=[];
    xdata(ofScal)=[];

    if length(xdata)<5
        warning(['Significant amount of data is off-scale in file ' cell2mat(peakZ(1).fileZ(n).name)]);
        peakZ(peakNum).fileZ(n).pkbnd=[1 3];
        peakZ(peakNum).fileZ(n).pkrange=[1 2 3];
        peakZ(peakNum).fileZ(n).area=-1;
        peakZ(peakNum).fileZ(n).gParams=[0 1 1 0 0 0];
    else
        lb=[-inf,min(xdata)+range(xdata)/12,1.5,-inf,-inf];
        ub=[ inf,max(xdata)-range(xdata)/12,range(xdata)/2,inf,inf];
        if isempty(cPeak)
            cPeak=peakZ(peakNum).peakId;
        end

        vecslope=(ydata(end)-ydata(1))/(xdata(end)-xdata(1));
        cydat=(ydata-ydata(1))-vecslope*(xdata-xdata(1));
        [pkheight,pkcenter]=max(cydat);
        if ~isempty(cPeak)

            pkwid=cPeak.pkwid;
            % Peak center isnt a good variable
            %  lb(2)=cPeak.pkcenter-cPeak.pkvar;
            %  ub(2)=cPeak.pkcenter+cPeak.pkvar;
            lb(3)=cPeak.pkwid-cPeak.widvar;
            up(3)=cPeak.pkwid+cPeak.widvar;
        else

            pkwid=range(xdata)/6;

        end
        OPTIONS=optimset('MaxFunEvals',90000,'MaxIter',1200,'Display','off');
        gParamsz=[pkheight(1),xdata(pkcenter(1)),pkwid,vecslope,ydata(1)-vecslope*xdata(1)];
        if prod(double((ub-lb)>0))==1
            [gParams,bulldog,resid]=lsqcurvefit(@gaussfun,gParamsz,xdata,ydata,lb,ub,OPTIONS);
        else
            gParams=gParamsz;
        end
        signal=gaussfun(gParams,xdata);
        isignal=gaussfun(gParamsz,xdata);
        noise=abs(ydata-signal);
        s2n1=(signal.^2)./(noise.^2);
        s2n2=sqrt(sum(s2n1));
        s2n3=s2n2/length(noise);
        s2n=20*log(s2n3);
        disp(['SNR #' num2str(n) ' {Shift, Gaussian}: {' num2str(round(ssnr*10)/10) ' , ' num2str(round(s2n*10)/10) '}']);
        %figure;plot(xdata,ydata,'r+');hold on;plot(xdata,signal,'b-');plot(xdata,isignal,'g-');
        %line([gParams(2) gParams(2)],[min(ydata) max(ydata)]);
        %line([gParams(2)-gParams(3) gParams(2)],[min(ydata) max(ydata)]);
        %line([gParams(2)+gParams(3) gParams(2)],[min(ydata) max(ydata)]);
        %pause(1);hold off;close;
        % code to plot the gaussian on the real graph
        plot(xdata,signal+n*peakZ(1).plotsep,'k-');

        peakZ(peakNum).fileZ(n).gParams=[gParams s2n];
        kn1=max(gParams(1)/(sum(noise)/length(noise)));
        kn2=max(abs(((x1+x2)/2-gParams(2))/gParams(3)));
        disp(['Slidin Check: H2N ' num2str(kn1) ' DRFT-' num2str(kn2)]);
        disp(['Slidin Check: DD ' num2str(abs((x1+x2)/2-gParams(2)))]);
        pFixed=0;
        if (isempty(peaky) | dumAss) % the peak cant be fixed it its just following orders
            if s2n>TOLSNR & kn1>TOLHEIGHT % make sure peak is big enough and nice enough
                if kn2>TOLDRIFT % make sure the peak has drifted enough from the center of the datapts
                    % redo x1,x2,refdata
                    warning('Peak Appears to be drifting away from the center, correcting');
                    x1=gParams(2)-gParams(3)*6;
                    x2=gParams(2)+gParams(3)*6;
                    ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
                    ss2=max(find(abs(peakZ(1).fileZ(n).x-x2)==min(abs(peakZ(1).fileZ(n).x-x2))));

                    peakZ(peakNum).refData=peakZ(1).fileZ(n).x(ss1:ss2);
                    peakZ(peakNum).fileZ(n).pkbnd=[x1 x2];
                    peakZ(peakNum).fileZ(n).pkrange=[ss1:ss2];
                    peakZ(peakNum).fileZ(n).area=pkInt(peakZ(1).fileZ(n),[ss1:ss2]);
                    [peakZ,xTemp]=doPeak(peakZ,[x1 x2],peakNum,n);
                    pFixed=1;
                end
            end
        end
        if pFixed==0
            c1=[peakZ(1).fileZ(n).x(ss2+shft)];
            c2=[n*peakZ(1).plotsep+peakZ(1).fileZ(n).y(ss2+shft)];

            plot(c1,c2,[colorZ '+'],'MarkerSize',2)
        end

    end
end
try
    % end gaussian
catch
    warning(['Something just fischy about the data ' cell2mat(peakZ(1).fileZ(n).name)]);
    peakZ(peakNum).fileZ(n).pkbnd=[1 3];
    peakZ(peakNum).fileZ(n).pkrange=[1 2 3];
    peakZ(peakNum).fileZ(n).area=-1;
    peakZ(peakNum).fileZ(n).gParams=[0 1 1 0 0 0];
end

function area=pkInt(cpeak,range)
% code to integrate using the trapezoidal method the peak and subtract the baseline
x=cpeak.x(range);
y=cpeak.y(range);
m=(y(end)-y(1))/(x(end)-x(1));
baseline=m*(x-x(1))+y(1);
area=rawInt(x,y,baseline);

function area=rawInt(x,y,baseline);
if isnan(baseline)
    % only occurs when the peak consists of one point, but should a
    % possiblity nonetheless
    area=0;
else
    if prod(double(size(y)==size(baseline)))
        area=trapz(x,y-baseline);
    else
        area=trapz(x,y-baseline');
    end
end
% allow negative area
%if area<0
%    area=0;
%end

function [shft,jed,snr]=jaque(refpeak,cgraph,rng,ns,boundZ)
% code to make sure it isnt shifting the peak outside of the data
% boundaries
nsdown=ns;
nsup=ns;
if (rng(1)-nsdown)<min(boundZ)
    nsdown=abs(min(boundZ)-(rng(1)));
    % the downswing is equal to the difference between the minimum value
    % and the beginning
end
if (rng(end)+nsup)>max(boundZ)
    nsup=max(boundZ)-rng(end)-1;
    % the upswing is equal to the maximum value minus the end of the range
    % -1
end
n=-nsdown;
refpeak=refpeak-refpeak(1);
j={};
%figure
while n<=nsup
    cpeak=cgraph(rng+n);
    cpeak=cpeak-cpeak(1);
    % mfact is the factor you can multiply the current peaks area by to
    % match that of the reference peaks
    mfact=max(refpeak)/max(cpeak);
    % mfact is limited to .25 to 4 or else noise can get amplified to look
    % like a peak
    mmax=4;
    if ns>1000
        mmax=10;
    end
    if mfact<inv(mmax)
        mfact=inv(mmax);
    end
    if mfact>mmax
        mfact=mmax;
    end
    cpeak=mfact*cpeak;
    % find the difference between the reference peak and the current peak
    j(end+1).jval=sqrt(sum((refpeak-cpeak).^2)/length(cpeak));
    j(end).offset=n;


    n=n+1;
end
if isempty(j)
    shft=0;
    resid=1e9;
    jed.jval=1e10;
    jed.offset=0;
else
    [alber shft]=min([j.jval]);
    resid=j(shft).jval;
    shft=j(shft).offset;
    jed=j;
end
if resid==0
    resid=0.1;
end
snr=20*log(sqrt(sum(refpeak.^2)/length(refpeak))/resid);
% disp(['Shift S/N: ' num2str(snr)]);


function pk2 = pkfind(x,p2)
%PKFind
%
%  pk = pkfind(x,p)   peak hunt down using peak half window width of p.
%
% Returns PK, a list of values of indices of the peak max
%
x=x(:);
p2=floor(p2);
p = 2*p2 + 1;          % full window width
[n,m] = size(x);
z = zeros(p2,m);
j = toeplitz(p:-1:1,p:n);    % shift operator
%
y = zeros(p,(n-p+1)*m);
y(:) = x(j,:);
ma = max(y);                 % find maximum in window
%
pk = [z ; reshape(ma,n-p+1,m) ; z]; % add missing edge elements
pk = pk == x;                 % find matching elements
%%
pk2=find(pk);

function loadMaster
% This function loads a master list of peak information from an easily
% editable excel file the information includes the peaks name, the peaks
% center and a plethora of other information
global MasterPeak
MasterPeak={};
if length(dir([pwd '\MasterRetentionList.xls']))>0
    [pkdat,names]=xlsread([pwd '\MasterRetentionList.xls']);
else
    [pkdat,names]=xlsread('MasterRetentionList.xls');
end
pknm=names([2:end],1)
for i=[1:length(pknm)]
    MasterPeak(end+1).name=cell2mat(pknm(i));
    MasterPeak(end).pkcenter=pkdat(i,1);
    MasterPeak(end).pkvar=pkdat(i,2);
    MasterPeak(end).pkwid=pkdat(i,3);
    MasterPeak(end).widvar=pkdat(i,4);
    MasterPeak(end).visible=pkdat(i,5);
end

function [pkName,cPeak]=identifyPeak(xd)
global MasterPeak
PKCNT=4;
fakePeakWidth=range(xd)/6; % a rough guess of the peaks half width
peakCenter=sum(xd)/2; % the peaks center
% first step find the closest to the peak center
dArr=abs(peakCenter-[MasterPeak.pkcenter]);
[junk,twist]=sort(dArr);
likely=twist([1:PKCNT]);
% Formula for figuring out how likely you have a match

%Center Portion
cArr=([MasterPeak(likely).pkvar]-abs([MasterPeak(likely).pkcenter]-peakCenter))./[MasterPeak(likely).pkvar];
cFactor=.99+(double(cArr<0).*cArr);
cFactor=double(cFactor>.1).*cFactor+.01;

%Width Portion
wArr=([MasterPeak(likely).widvar]-abs([MasterPeak(likely).pkwid]-fakePeakWidth))./[MasterPeak(likely).widvar];
wFactor=.99+(double(wArr<0).*wArr);
wFactor=double(wFactor>.1).*wFactor+.01;
%Likelyhood Peak actually appears
pFactor=[MasterPeak(likely).visible];

rankz=(5*cFactor+pFactor)/7;
[junk,twist]=sort(1./rankz);
% hugeWaste;
pkz={};
for ij=[1:length(twist)]
    pkz(end+1)={[MasterPeak(likely(twist(ij))).name ' - Likelyhood: ' num2str(1/junk(ij)*100) '%']};
end
pkz(end+1)={'Other...'};
pkSel=menu('Identify what peak this is',pkz);
if pkSel==length(pkz)
        cRoot=peakCenter;
        cN=zpad(cRoot,4);
    pkName=[cN '.pks'];
    cPeak={};
else
    cPeak=MasterPeak(likely(twist(pkSel)));
    pkName=cPeak.name;
end

function f=gaussfun(A,x)
f = A(1)*exp(-0.5.*((x-A(2))/A(3)).^2)+A(4)*x+A(5);

function cN=zpad(cRoot,digz)
cN=num2str(round(cRoot));
while length(cN)<digz
    cN=['0' cN];
end

function moldyCode
% old code im too attached to too delete
if getpklist==3;
    linehandles=slidestack(peakZ)
    uiwait(msgbox('Select Graph Using Arrow Keys'))
    pks=inputdlg('About How Many Peaks Should be Found? (Overestimate if nessecary)');
    pks=str2num(cell2mat(pks));
    if 1<pks<30
        % user gave a valid number
    else
        % user is messing with me
        msgbox('Go away, your only getting 16 peaks')
    end
    [xd,flecnt,pkstr]=slidepick;
    x1=xd(1);
    x2=xd(2);
    % find what x-value the user probably clicked
    s1=find(abs(peakZ(1).fileZ(flecnt).x-x1)==min(abs(peakZ(1).fileZ(flecnt).x-x1)));
    s2=find(abs(peakZ(1).fileZ(flecnt).x-x2)==min(abs(peakZ(1).fileZ(flecnt).x-x2)));
    s3=s2-s1;
    refpeak=peakZ(1).fileZ(flecnt).y(s1:s2);
    pause(.2);
    cpks=0;
    wdth=100;
    jjkz=0;
    while abs(cpks-pks)>2 & jjkz<15
        fpks=pkfind(peakZ(1).fileZ(flecnt).y,wdth);
        cpks=length(fpks);
        if cpks>pks
            wdth=wdth*1.25;
        else
            wdth=wdth*.75;
        end
        jjkz=jjkz+1;
    end
    plot(peakZ(1).fileZ(flecnt).x(fpks),peakZ(1).fileZ(flecnt).y(fpks)+flecnt*peakZ(1).plotsep,'ro');
    refpeak=refpeak-refpeak(1);
    % width-range 50-1000 pts
    swid=25;
    ewid=500;
    kval=[swid:2:ewid];
    for k=1:length(fpks)
        bmat=[];
        for kwid=kval
            cng=[fpks(k)-kwid;fpks(k)+kwid];
            if cng(1)<1
                cng(1)=1;
            end
            if cng(2)>length(peakZ(1).fileZ(flecnt).y)
                cng(2)=length(peakZ(1).fileZ(flecnt).y);
            end
            cdat=peakZ(1).fileZ(flecnt).y(cng(1):cng(2));
            rdat=interpft(refpeak,range(cng)+1);
            cdat=cdat-cdat(1);
            mfact=max(rdat)/max(cdat);
            if mfact>10
                mfact=10;
            elseif inv(mfact)>10
                mfact=inv(10);
            end
            cdat=cdat.*mfact;
            bmat=[bmat sum(abs(cdat-rdat))/length(cdat)];
        end
        [apple,orange]=min(bmat);
        jjz=orange(1);
        cng=[fpks(k)-kval(jjz);fpks(k)+kval(jjz)];
        if cng(1)<1
            cng(1)=1;
        end
        if cng(2)>length(peakZ(1).fileZ(flecnt).y)
            cng(2)=length(peakZ(1).fileZ(flecnt).y);
        end
        peakZ=slider(peakZ,peakZ(1).fileZ(flecnt).x(cng),flecnt,pkstr,{});
        peakZ(end).peakGroup=-1;
    end
    getpklist=1;

end

if getpklist == 2;
    % uiload brings everything in, we only want the peaklist not the data
end