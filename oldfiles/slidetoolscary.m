function slidetools
global MasterPeak;
global SLIDEVERSION;
global linehandles;
global manu;
global help;
global lstpeak;
global baroJump;
global SlideFigure;
% #function dealKey
lstpeak=1;
baroJump=0;
SlideFigure=-1;
% 1 is a placeholder year, month, day
SLIDEVERSION=1060717;
slideversionD=SLIDEVERSION;
help=0;
warning off MATLAB:divideByZero
MasterPeak={};
loadMaster;
norres=menu('How to Begin','New Integration','Resume an Old Integration');
if norres==1
    % show list of available files, return the selected ones
    selectedfiles = vocgetfiles;
    % setup the columns

    %tcol=menu('What Column is Time?','Column 1','Column 2','No Time Column');
    dcol=menu('Where are the data?','Column 1','Column 2','Column 3','Column 4');
    % load the data
    peakZ={};
    L=1;
    while L<=length(selectedfiles)
        % prep the data before choosing the peaks
        try
            [data, hdrinfo(L),time] = vocdataprep(selectedfiles(L),0);

            %[header, data] = vocload(char(selectedfiles(L)));
            %hdrinfo(L) = vocheadersProto(header);
            if size(data,1)>50 & size(data,2)>0
                peakZ(1).fileZ(L).name=selectedfiles(L);

                peakZ(1).fileZ(L).date=hdrinfo(L).date;
                %if tcol==3
                % generate a x column of just datapoints
                %    peakZ(1).fileZ(L).x=[1:length(data(:,dcol))];
                %else
                % peakZ(1).fileZ(L).x=data(:,tcol);
                % end
                peakZ(1).fileZ(L).x=time;
                peakZ(1).fileZ(L).y=data(:,dcol);
                peakZ(1).fileZ(L).area=0;
                peakZ(1).fileZ(L).pkbnd=[];
                peakZ(1).fileZ(L).gParams=[];
                peakZ(1).peakRoot=0;
                peakZ(1).peakStr=0;
                peakZ(1).datatype=0;
                peakZ(1).peakGroup=-1;
                peakZ(1).pkName='';
                peakZ(1).peakId={};
                peakZ(1).peakMisc.fitFun='@gaussfun';
                peakZ(1).peakMisc.peakFollow=''; % peakName to follow

                peakZ(1).plotsep=sum(data(:,1))/length(data(:,1))/7;
                peakZ(1).refData=[];
                L=L+1;
            else % data is TOo Small
                disp(['Data file ' cell2mat(selectedfiles(L)) ' is not valid or too small']);
                selectedfiles(L)=[];
                hdrinfo(L)=[];
            end
        catch
            disp(['Data file ' cell2mat(selectedfiles(L)) ' is not even remotely valid']);
            selectedfiles(L)=[];

        end
    end
    % begin peak selection part of analysis
end
if norres==2
    disp('Now loading data file ...');
    [SLIDEVERSION,peakZ]=fullLoad;
    if exist('SLIDEVERSION')
        if SLIDEVERSION<1050712
            uiwait(msgbox('This data file you selected to load is not very compatible with the current version. No conversion can take place using the resume integration method. If you want to recover this data. Create a new integration with the same datafiles and load this file as a peaklist','Fatal Error','modal'));
            return
        elseif SLIDEVERSION<1050821
            oldData=1;
        elseif SLIDEVERSION<1060608
            disp('Adding Distortion Factor');
            for k=2:length(peakZ)
                for j=1:length(peakZ(k).fileZ)
                    cPeak=peakZ(k).fileZ(j);
                    cPeak.gParams=[cPeak.gParams(1:3) 1 cPeak.gParams(4:end)];
                    peakZ(k).fileZ(j)=cPeak;
                end
            end
        else
            oldData=0;
            'okey dokey'
        end
    else
        uiwait(msgbox('This data file you selected to load is not very compatible with the current version. No conversion can take place using the resume integration method. If you want to recover this data. Create a new integration with the same datafiles and load this file as a peaklist','Fatal Error','modal'));
        return
    end
    SLIDEVERSION=slideversionD;
    peakZ=sortpeaks(peakZ);
    % getpklist = menu('Analysis Menu','Use only existing peaks','Load a peaklist into file','Back');
    % if getpklist==3
    %    getpklist==4;
    %end
    disp('Finished Loading Data')
    getpklist=1;

end
close;
% get ready for the slider
linehandles=slidestack(peakZ);
manu=0;
while manu>-1
    pause(.2);
    if manu==1.1 % load le list
        disp('Now loading peaklist file ...');
        [peaklist,pooknums,fver,vodka,isTool,wexCel]=safeload;
        oldver=0;
        if isempty(fver)
            uiwait(msgbox('This data file you selected to load is not very compatible with the current version. A conversion will take place, but expect peaks to have moved.','Warning','modal'));
            oldver=1;
        end

        %close;
        % get ready for the slider
        %linehandles=slidestack(peakZ);
        n=1;
        nsel=1;

        openz=1;
        if menu('Turn off graphic output while running?','Yes','No')==1
            close all;
            openz=0;
        end
        doGroupz=1-(menu('Integrate groups in new file?','Yes','No')-1);
        porder=peaklistOrder(pooknums,isTool);
        for k=porder
            pizeakFollow=cell2mat(isTool(k));

            peakZ(end+1)=peakZ(end);
            peakZ(end).fileZ(1).pkbnd=[peaklist(k,[1,2])];
            for jk=1:length(peakZ(end).fileZ)
                peakZ(end).fileZ(jk).x=[];
                peakZ(end).fileZ(jk).y=[];
            end
            peakZ(end).peakRoot=[peaklist(k,[1,2])];
            peakZ(end).peakStr=peaklist(k,3);
            peakZ(end).peakGroup=peaklist(k,4);
            peakZ(end).peakId={};
            peakZ(end).pkName=str2mat(pooknums(k));
            peakZ(end).refData=cell2mat(vodka(k));
            try
                peakZ(end).peakId=cell2mat(wexCel(k));
            catch
                peakZ(end).peakId={};
            end
            peakZ(end).peakMisc.peakFollow=pizeakFollow;
            peakZ(end).peakMisc.gaussfun='@gaussfun';
            peakZ=slideSearch(peakZ,[peaklist(k,[1,2])],1);
            pause(.5);
            exportResults(peakZ,1);

        end
        peakZ=sortpeaks(peakZ);

        if openz==0
            linehandles=slidestack(peakZ);
        end
        % code to complete all the groups
        if doGroupz==1
            %fuck
            for gp=[1:max(peaklist(:,4))]
                peakZ=doPeakGroup(peakZ,gp,[1:length(peakZ(1).fileZ)]);
            end
        end
        manu=0;
        disp('Finished Loading Peaklist')
    end

    if manu==1
        [xd,yd,zd]=ginput(2);
        if prod(zd)==1
            [pkName,cPeak]=identifyPeak(xd);
            
            if ~isempty(cPeak)
                origName=1;
                done=0;
                while ~done
                    for k=2:length(peakZ)
                        origName=origName & ~strcmp(peakZ(k).pkName,pkName);
                    end
                    if origName
                        done=1;
                    else
                        pkName=[pkName '.1'];
                        origName=1;
                    end
                end
            end
            pkstr = menu('Search Range','    High (Low Peak Density, High Peak Strength)    ','   Medium(High Peak Density or Low Peak Strength)    ','     Low (High Peak Density and Low Peak Strength)     ','     SuperLow (High Peak Density and Low Peak Strength)     ','     Zero (Drifting Feature Disabled)     ');
            if pkstr==1
                pkstr=400;
            elseif pkstr==2
                pkstr=150;
            elseif pkstr==3
                pkstr=75;
            elseif pkstr==4
                pkstr=30;
            elseif pkstr==5
                pkstr=0;
            end
            grplst=grpList(peakZ);
            grp=menu('Peak Group',grplst);
            if grp==1
                grp=-1;
            else
                grp=grp-1;
            end
            peakslist={'None'};
            for k=2:length(peakZ)
                peakslist(k)={peakZ(k).pkName};
            end
            if length(peakZ)>1
                jpk=menu('Follow a specific Peak?',peakslist);
            else
                jpk=1;
            end

            peakZ=slider(peakZ,xd,lstpeak,pkstr,cPeak,peakZ(jpk).pkName,pkName,grp);

            % peakZ(1)
            % peakZ(end).peakGroup=grp;


            peakZ=sortpeaks(peakZ);
            exportResults(peakZ,1);
            disp('Finished Integrating Peak')
        end
        manu=0;
    end
    if manu==666
        global dyet;
        peakP={};
        dyet=0;
        uiwait(msgbox('Select Chromatogram with that has the peak you are interested in using the Arrow Keys on the keyboard and then click ok'));
        speak=lstpeak;
        uiwait(msgbox('Select Peak Using Mouse'));
        [xd,yd,zd]=ginput(2);
        peakP.peakStr=0;
        peakP.peakId.pkcenter=100;
        peakP.peakId.pkvar=10;
        peakP.peakId.pkwid=4;
        peakP.peakId.widvar=20;
        peakP.peakId.df=1;
        peakP.peakId.dfvar=2.5;
        peakP.plotsep=0;
        peakP.peakMisc.peakFollow='';
        peakP.fileZ=peakZ(1).fileZ(speak);
        peakP.peakRoot=xd;
        peakP.peakGroup=-1;
        
        x1=min(xd);
        x2=max(xd);
        s1=find(abs(peakP.fileZ.x-x1)==min(abs(peakP.fileZ.x-x1)));
        s2=find(abs(peakP.fileZ.x-x2)==min(abs(peakP.fileZ.x-x2)));
        peakP.refData=[peakP.fileZ.y(s1:s2)];
        
        peakP(2)=peakP(1);
        h=figure;
        set(h,'DeleteFcn','global dyet;dyet=-1;')
        hold on;
        
        peakP=doPeak(peakP,xd,2,1);
        cydat=peakP(1).refData - (peakP(2).fileZ.gParams(5).*peakP(1).fileZ.x(s1:s2));
        s1=max([find(abs(peakP(1).fileZ.x-x1)==min(abs(peakP(1).fileZ.x-x1)))-40,1]);
        s2=min([find(abs(peakP(1).fileZ.x-x2)==min(abs(peakP(1).fileZ.x-x2)))+40,length(peakP(1).fileZ.x)]);

        plot(peakP(1).fileZ.x(s1:s2),peakP(1).fileZ.y(s1:s2));
        for k=1:6
            disp(num2str(peakP(2).fileZ.gParams(k)));
        end
        text(min(xd)+range(xd)*3/8,max(peakP(1).refData),['Peak Height: ' num2str(peakP(2).fileZ.gParams(1))]);
        text(min(xd)+range(xd)*3/8,min(peakP(1).refData)+range(peakP(1).refData)/4,['Peak Retention Time: ' num2str(peakP(2).fileZ.gParams(2))]);
        text(min(xd)+range(xd)*3/8,min(peakP(1).refData)+range(peakP(1).refData)/2,['Peak Half Width: ' num2str(peakP(2).fileZ.gParams(3))]);
        text(max(xd)-range(xd)/3,max(peakP(1).refData)-range(peakP(1).refData)/3,['Peak Distortion: ' num2str(peakP(2).fileZ.gParams(4))]);
        text(min(xd),max(peakP(1).refData),['Peak Area: ' num2str(gArea(peakP,2,1))]);
        text(min(xd),max(peakP(1).refData)-range(peakP(1).refData)/2,['Data STD: ' num2str(round(std(cydat)))])
        
        %while dyet==0
            pause(.5);
       % end
       ePort=menu('Would you like to Place this Peak in the Excel File?','Yes','No');
       if ePort==1
            prepArray=[peakP(2).fileZ.gParams(2) 1 peakP(2).fileZ.gParams(3) 1 1 peakP(2).fileZ.gParams(4) .1];
            
            prepArray(2)=.2*prepArray(1);
            prepArray(4)=.2*prepArray(3);
            prepArray(7)=.05*prepArray(6);
            writeExcel(prepArray,[num2str(peakP(2).fileZ.gParams(2)) '.pks']);
       end
        manu=0;
    end
    if manu==55
        peakZ=sortpeaks(peakZ);
        manu=0;
    end
    if manu==3271
        peakZ=xcelInteg(peakZ);
        manu=0;
    end
    if manu==99
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k-1)={peakZ(k).pkName};
        end
        [s,v] = listdlg('PromptString','Select Peaks to delete:','ListString',peakslist);
        if v==1
            hy=menu('Are you sure you want to delete selected peak(s)?','Yes','No');
            if hy==1
                close;

                peakZ(s+1)=[];

                % redraw all data without the peak
                linehandles=slidestack(peakZ);
                % handles = makemenu(gcf, labels, calls);
            end
        end
        manu=0;
    end
    if manu==4422
        % re-rail code
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k)={peakZ(k).pkName};
        end
        jpk=menu('Which Peak?',peakslist);
        if jpk>1
            
            inGroup=[]
            hbo=0;
            [garb,complete]=grpList(peakZ);
            
            if ~isempty(peakZ(jpk).peakMisc.peakFollow)
                hbo=menu('Are you sure you wish to rerail a follower peak?','Yes','No');
                if hbo==2
                    jpk=1;
                end

            end
            if peakZ(jpk).peakGroup>0
                inGroup=peakZ(jpk).peakGroup
            end
            uiwait(msgbox('Select Starting Chromatogram with that has the peak you want to be rerailed using the Arrow Keys on the keyboard and then click ok','SlideTools:ReRail 2/7'));
            speak=lstpeak;
            [xd yd]=ginput(2);
            x1=min(xd);
            x2=max(xd);
            if hbo==1
                peakZ(jpk).peakRoot=[x1 x2];
                for npn=[2:length(peakZ)]
                    if strcmp(peakZ(jpk).peakMisc.peakFollow,peakZ(npn).pkName);
                        peakZ(npn).peakRoot=peakZ(npn).fileZ(speak).pkbnd;
                    end
                end
            end
            uiwait(msgbox('Select Finishing Chromatogram with that has the peak you want to be rerailed using the Arrow Keys on the keyboard and then click ok','SlideTools:ReRail 3/7'));
            epeak=lstpeak;
            pkstr = menu('Search Range','    High (Low Peak Density, High Peak Strength)    ','   Medium(High Peak Density or Low Peak Strength)    ','     Low (High Peak Density and Low Peak Strength)     ','     SuperLow (High Peak Density and Low Peak Strength)     ','     Zero (Drifting Feature Disabled)     ');
            if pkstr==1
                pkstr=400;
            elseif pkstr==2
                pkstr=150;
            elseif pkstr==3
                pkstr=75;
            elseif pkstr==4
                pkstr=30;
            elseif pkstr==5
                pkstr=0;
            end
            % uiwait(msgbox('Select Peak'));
            doRedraw=menu('Would you like to redraw the entire graph after rerailing is finish (Yes is recommended)?','Yes','No');
            refollow=menu('Would you like to Re-Rail the Peaks Following the current peak (Yes is recommended)?','Yes','No');
            % find what x-value the user probably clicked
            s1=find(abs(peakZ(1).fileZ(speak).x-x1)==min(abs(peakZ(1).fileZ(speak).x-x1)));
            s2=find(abs(peakZ(1).fileZ(speak).x-x2)==min(abs(peakZ(1).fileZ(speak).x-x2)));
            oleRef=peakZ(jpk).refData; % used if the peak doesnt include the last peak
            peakZ(jpk).refData=peakZ(1).fileZ(speak).y(s1:s2);
            peakZ(jpk).pkbnd=[x1,x2];
            oleStr=peakZ(jpk).peakStr;
            peakZ(jpk).peakStr=pkstr;
            xtemp=[x1 x2];
            hold on;
            qd=sign(epeak-speak);
            if qd==0
                qd=1;
            end

            for n=[speak:qd:epeak]
                [peakZ xtemp]=doPeak(peakZ,xtemp,jpk,n);
                nextPeak={peakZ(jpk).pkName};
                stillRecur=1;
                if refollow==1
                    while length(nextPeak)>0 % re rail all followers
                        captPeak=cell2mat(nextPeak(end));
                        nextPeak(end)=[];
                        for npn=[2:length(peakZ)]
                            if strcmp(peakZ(npn).peakMisc.peakFollow,captPeak)
                                if peakZ(npn).peakGroup>0
                                    if sum(inGroup==peakZ(npn).peakGroup)<1
                                        inGroup(end+1)=peakZ(npn).peakGroup
                                    end
                                end
                                [peakZ bogus]=doPeak(peakZ,peakZ(npn).fileZ(n).pkbnd,npn,n);
                                nextPeak(end+1)={peakZ(npn).pkName};
                            end
                        end
                    end
                end

            end

            if epeak<length(peakZ)
                peakZ(jpk).peakStr=oleStr;
                peakZ(jpk).refData=oleRef;
            end
            for ab=inGroup
                if complete(ab)==1
                    peakZ=doPeakGroup(peakZ,ab,[speak:qd:epeak]);
                end
            end
            if doRedraw==1
                hold off;
                linehandles=slidestack(peakZ);
            end
        end
        disp('Finished ReRailing Peak')
        manu=0;
    end
    if manu==9989 % code to delete chromatograph
        flecnt=lstpeak;
        hy=menu('Are you sure you want to delete chromatogram?','Yes','No');
        if hy==1
            for abk=1:length(peakZ)
                peakZ(abk).fileZ(flecnt)=[]
            end
            if lstpeak>length(peakZ(1).fileZ)
                lstpeak=length(peakZ(1).fileZ);
            end
            linehandles=slidestack(peakZ);
            % handles = makemenu(gcf, labels, calls);
        end
        manu=0;

    end
    if manu==947
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k)={peakZ(k).pkName};
        end
        jpk=menu('Which Peak?',peakslist);
        if jpk>1
            uiwait(msgbox('Select Reference Graph Using Arrow Keys then click ok'));
            [xd yd]=ginput(2);
            flecnt=lstpeak;
            x1=xd(1);
            x2=xd(2);
            ss1=max(find(abs(peakZ(1).fileZ(flecnt).x-x1)==min(abs(peakZ(1).fileZ(flecnt).x-x1))));
            ss2=max(find(abs(peakZ(1).fileZ(flecnt).x-x2)==min(abs(peakZ(1).fileZ(flecnt).x-x2))));
            peakZ(jpk).fileZ(flecnt).pkbnd=[xd];
            peakZ(jpk).fileZ(flecnt).pkrange=[peakZ(1).fileZ(flecnt).x(ss1:ss2)];
            peakZ(jpk).fileZ(flecnt).area=pkInt(peakZ(1).fileZ(flecnt),[ss1:ss2]);
            hold on;
            plot(peakZ(1).fileZ(flecnt).x([ss1,ss2]),peakZ(1).fileZ(flecnt).x([ss1,ss2]),'kh');

        end
        manu=0;
    end
    if manu==77
        peakslist={'None'};
        for k=2:length(peakZ)
            peakslist(k)={[peakZ(k).pkName '  Group ' num2str(peakZ(k).peakGroup)]};
        end
        jpk=menu('Which Peak?',peakslist);
        grplst=grpList(peakZ);
        if jpk>1
            grp=menu('Peak Group',grplst)-1;
            if grp==0
                grp=-1;
            end
            peakZ(jpk).peakGroup=grp;

        end
        manu=0;
    end
    if manu==24 % group integrate Zippee
        grplst=grpList(peakZ);
        grp=menu('Peak Group',grplst)-1;
        if grp>0
            peakZ=doPeakGroup(peakZ,grp,[1:length(peakZ(1).fileZ)]);
            disp(['Finished Completing Group ' num2str(grp)])
        end
        manu=0;

    end

    if manu==6969
        keyboard;
        manu=0;
    end
    if manu==4455
        int2jpg(peakZ);
        disp('Finished Making JPGs')
        manu=0;
    end
    if manu==4411
        appendMaster(peakZ);
        manu=0;
    end
    if manu==9 | manu==-1
        exportResults(peakZ,0);
        if manu>0
            manu=0;
        end
    end
end
function peakZ=groupInt(peakZ)
groupCount=1;
n=1;
while groupCount>0
    groupie=[peakZ.peakGroup];
    groupie=find(groupie==n);
    groupCount=length(groupie);
    for ikj=1:length(peakZ(1).fileZ)
        rbnd=[];
        lbnd=[];
        for ijk=1:length(groupie)
            cng=peakZ(groupie(ijk)).fileZ(ikj).pkrange;
            lbnd=[lbnd;min(cng)];
            rbnd=[rbnd;max(cng)];
        end
        rbnd=max(rbnd);
        lbnd=min(lbnd);
        % rbnd=[peakZ(1).fileZ(ikj).x(rbnd) peakZ(1).fileZ(ikj).y(rbnd)];
        % lbnd=[peakZ(1).fileZ(ikj).x(lbnd) peakZ(1).fileZ(ikj).y(lbnd)];
        if ~isempty(rbnd)
            x=peakZ(1).fileZ(ikj).x(lbnd:rbnd);
            y=peakZ(1).fileZ(ikj).y(lbnd:rbnd);
            m=(y(end)-y(1))/(x(end)-x(1));
            baseline=m*(x-x(1))+y(1);

            for ijk=1:length(groupie)
                cng=peakZ(groupie(ijk)).fileZ(ikj).pkrange;
                %  figure
                %  plot(baseline(cng-lbnd+1));
                %  hold on;
                %  plot(peakZ(1).fileZ(ikj).y(cng))
                %  pause(.2);
                %  close
                peakZ(groupie(ijk)).fileZ(ikj).area=rawInt(peakZ(1).fileZ(ikj).x(cng),peakZ(1).fileZ(ikj).y(cng),baseline(cng-lbnd+1));
            end
        end
    end
    n=n+1;
end

function [grplst,complete]=grpList(peakZ)
grplst={'No Group'};
complete=[];
bozo=0;
grps=max([peakZ.peakGroup]);
for n=1:grps
    tempArr=[];
    allSame=1;
    tempStr=['Group '  num2str(n) ': '];
    for tappy=[2:length(peakZ)]
        if peakZ(tappy).peakGroup==n
            tempStr=[tempStr peakZ(tappy).pkName ' '];
            for k=1:length(peakZ(tappy).fileZ)
                if length(peakZ(tappy).fileZ(k).gParams)<7
                    peakZ(tappy).fileZ(k).gParams=[peakZ(tappy).fileZ(k).gParams(1:3) 1 peakZ(tappy).fileZ(k).gParams(4:end)];
                elseif length(peakZ(tappy).fileZ(k).gParams)>7
                   peakZ(tappy).fileZ(k).gParams=peakZ(tappy).fileZ(k).gParams(1:7);
                end
            end
            cur=reshape([peakZ(tappy).fileZ.gParams],7,length(peakZ(tappy).fileZ))';
            cur=cur(:,5:7);
            if isempty(tempArr)
                tempArr=cur;
            end
            allSame=allSame & prod(prod(double(tempArr==cur)));
        end
    end
    if allSame==1
        tempStr=[tempStr '(Completed)'];
    end
    complete(end+1)=allSame;
    grplst(n+1)={tempStr};
end
grplst(end+1)={['New Group']};
complete(end+1)=0;


function [peaklist,pooknums,SLIDEVERSION,vodka,toolshed,wexCel]=safeload
uiload;
str={peakZ(2:end).pkName};
[s,v] = listdlg('PromptString','Select Peaks to Load:','ListString',str);
if v==1
    peakZ=peakZ([1 s+1]);
end

peaklist=[];
pooknums={};
vodka={};
toolshed={};
wexCel={};
try
    for k=2:length(peakZ)
        peaklist=[peaklist;[peakZ(k).fileZ(end).pkbnd peakZ(k).peakStr peakZ(k).peakGroup]];
        pooknums(end+1)={peakZ(k).pkName};
        if isempty(peakZ(k).refData)
            x1=min(peakZ(k).peakRoot);
            x2=max(peakZ(k).peakRoot);
            ss1=max(find(abs(peakZ(1).fileZ(lstpeak).x-x1)==min(abs(peakZ(1).fileZ(lstpeak).x-x1))));
            ss2=max(find(abs(peakZ(1).fileZ(lstpeak).x-x2)==min(abs(peakZ(1).fileZ(lstpeak).x-x2))));
            peakZ(k).refData=peakZ(1).fileZ(lstpeak).y(ss1:ss2);
        end
        wexCel(end+1)={peakZ(k).peakId};
        vodka(end+1)={peakZ(k).refData};
        toolshed(end+1)={peakZ(k).peakMisc.peakFollow};
    end
catch
    uiwait(msgbox('File selected is not of compatible file-format with the current version of SlideTools'));
end



function [xd,flecnt,pkstr]=slidepick()
global lstpeak;
[xda yd]=ginput(2);
% order x-values
xd=[min(xda);max(xda)];
flecnt=lstpeak;



function linehandles=slidestack(peakZ)
global SlideFigure;
n=1;
close all;
SlideFigure=figure;
hold on;
linehandles=[];
while n<=length(peakZ(1).fileZ)
    % alternate colors
    pz=mod(n,5);
    if pz==0
        cr='r-';
    end
    if pz==1
        cr='b-';
    end
    if pz==2
        cr='g-';
    end
    if pz==3
        cr='c-';
    end
    if pz==4
        cr='m-';
    end
    h=plot(peakZ(1).fileZ(n).x,peakZ(1).fileZ(n).y+n*peakZ(1).plotsep,cr);
    if length(peakZ)>1
        for k=2:length(peakZ)
            % pull in coordinates from the peakZ file
            x1=peakZ(k).fileZ(n).pkbnd(1);
            x2=peakZ(k).fileZ(n).pkbnd(2);
            ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
            ss2=max(find(abs(peakZ(1).fileZ(n).x-x2)==min(abs(peakZ(1).fileZ(n).x-x2))));
            plot(peakZ(1).fileZ(n).x(ss1),peakZ(1).fileZ(n).y(ss1)+n*peakZ(1).plotsep,'mo','MarkerSize',2);
            plot(peakZ(1).fileZ(n).x(ss2),peakZ(1).fileZ(n).y(ss2)+n*peakZ(1).plotsep,'m+','MarkerSize',2);
            if ~isempty(peakZ(k).fileZ(n).gParams) % plot gaussians with everything else
                slxdata=peakZ(1).fileZ(n).x(ss1:ss2);
                gUnit=peakZ(k).fileZ(n).gParams;
                gdata=gaussfun(gUnit(1:6),slxdata)+n*peakZ(1).plotsep;
                plot(slxdata,gdata,'k-')
            end
        end
    end
    linehandles=[linehandles;h];
    n=n+1;
end
title(peakZ(1).fileZ(1).name,'Interpreter','None');
set(gcf,'KeyPressFcn',['dealKey(((get(gcf,''CurrentCharacter'')+1)/2-.5)*2);']);
set(gcf,'DeleteFcn','global manu;manu=-1;')
labels = str2mat( ...
    '&Peak Menu', ...
    '>Select &Peak^D', ...
    '>&Batch Peaks', ...
    '>&Get Peak Parameter', ...
    '>&Delete Peak', ...
    '>&Change Peak Group', ...
    '>&Edit Specific Peak', ...
    '>Dele&te Specific Chromatograph', ...
    '>&Re-Rail Peak', ...
    '>C&omplete Group', ...
    '>Create &Images', ...
    '>&Export Peaks to Excel File', ...
    '>&Load Peaklist...', ...
    '>&Save Progress', ...
    '>E&xit' ...
    );
calls = str2mat( ...
    '', ...
    'global manu; manu=1;', ...
    'global manu; manu=3271;', ...
    'global manu; manu=666;', ...
    'global manu; manu=99;', ...
    'global manu; manu=77;', ...
    'global manu; manu=947;', ...
    'global manu; manu=9989;', ...
    'global manu; manu=4422;', ...
    'global manu; manu=24;', ...
    'global manu; manu=4455;', ...
    'global manu; manu=4411;', ...
    'global manu; manu=1.1;', ...
    'global manu; manu=9;', ...
    'global manu; manu=-1;' ...
    );
handles = makemenu(gcf, labels, calls);


function [peakZ]=sortpeaks(peakZ)
[null numpeaks] = size(peakZ); %find the number of peaks that have been selected

%for L=1 : numpeaks %get all the left sides of the root peaks in an array to sort
%    tosort(L) = sum(peakZ(L).peakRoot)/2;

%end
tosort={peakZ.pkName}
[leftpeak perm] = sort(tosort); %sort them
peakZ=peakZ(perm);
disp('Peak list sorted....')


function exportResults(peakZ,autom)
global lstpeak;
global SLIDEVERSION;
try
    if autom==1
        % save 'temp.mat';
        pathname=pwd;
        filename='tempFile.csv';
        filterindex=2;
        drm=',';
    else
        % uisave;
        [filename, pathname, filterindex] = uiputfile({'*.csv','Comma Seperated Values (*.csv)';'*.txt',  'Tab Delimited File (*.txt)'},'Save','integration.csv');
        if filterindex==2
            drm='\t';
        elseif filterindex==1
            drm=',';
        else
            return
        end
    end

    peakZ=groupInt(peakZ);

    %drm=',';
    saveDir=pwd;
    cd(pathname);
    eval(['save ' filename(1:end-4)  '.mat']);
    % pathname=[];
    % filename=input('Filename for area table ==>  ','s');

    % basically chad's output code
    % write a bunch of files, append if file exists
    % create the datatable with filenames, retention time, and corrosponding peak area
    disp('Now writing data file ...')


    fid1 = fopen([filename],'w');
    % must omit to allow unmix to read
    fprintf(fid1,['FILENAME' drm]);
    fprintf(fid1,['DAY OF WEEK' drm]);
    fprintf(fid1,['DATE/TIME' drm]);

    % write names for each top of each column
    for columnnames = 2:length(peakZ);
        cRoot=sum(peakZ(columnnames).peakRoot)/2;
        cN=zpad(cRoot,4);
        if ~isfield(peakZ(columnnames),'pkName')
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        if isempty(peakZ(columnnames).pkName)
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        fprintf(fid1,['%s' drm],peakZ(columnnames).pkName);
    end

    fprintf(fid1,'TOTAL\n');

    % write out the filenames as the leftmost column
    for j = 1:length(peakZ(1).fileZ);

        % titles of original chromatogram files(omit for unmix friendly version)
        % filter the names from the network cockygobble
        b=char(peakZ(1).fileZ(j).name);
        b=regexp(b,'.*?(?<filename>\w*[.]\w*)','names');
        b=b(end).filename;
        fprintf(fid1,['%s' drm],b);
        if SLIDEVERSION<1051019
            peakZ(1).fileZ(j).date=hdrinfo(j).date;
        end
        try
            [dayoweek, dayt] = strtok(peakZ(1).fileZ(j).date,',');

            dateloc = findstr(dayt,'-');
            dayt = dayt(dateloc(1)-2:dateloc(end));
            time = voctimecode(char(peakZ(1).fileZ(j).name));
            dayt =  strcat(dayt,time);
        catch
            dayoweek='';
            dayt='';
        end
        fprintf(fid1,['%s' drm],dayoweek);
        fprintf(fid1,['%s' drm], dayt);

        % write out the data into its correct location
        pkArez=0;
        for k = 2:length(peakZ);
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).area);
            % calcuate the TOTAL column
            pkArez=pkArez+peakZ(k).fileZ(j).area;
        end
        fprintf(fid1,['%12.4f' drm],pkArez);
        fprintf(fid1,'\n');
    end
    fclose(fid1);
    disp('Now writing gauss file ...')
    fid1 = fopen([filename(1:end-4) 'g' filename(end-3:end)],'w');
    % must omit to allow unmix to read
    fprintf(fid1,['FILENAME' drm]);
    fprintf(fid1,['DAY OF WEEK' drm]);
    fprintf(fid1,['DATE/TIME' drm]);

    % write names for each top of each column
    for columnnames = 2:length(peakZ);
        cRoot=sum(peakZ(columnnames).peakRoot)/2;
        cN=zpad(cRoot,4);
        if ~isfield(peakZ(columnnames),'pkName')
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        if isempty(peakZ(columnnames).pkName)
            peakZ(columnnames).pkName=[cN '.pks'];
        end
        fprintf(fid1,[peakZ(columnnames).pkName drm peakZ(columnnames).pkName '-H' drm peakZ(columnnames).pkName '-HW' drm peakZ(columnnames).pkName '-DF' drm peakZ(columnnames).pkName '-A' drm peakZ(columnnames).pkName  '-TA' drm peakZ(columnnames).pkName  '-S2N' drm]);
    end
    fprintf(fid1,'\n');
    % write out the filenames as the leftmost column
    for j = 1:length(peakZ(1).fileZ);

        % titles of original chromatogram files(omit for unmix friendly version)
        % filter the names from the network cockygobble
        b=char(peakZ(1).fileZ(j).name);
        b=regexp(b,'.*?(?<filename>\w*[.]\w*)','names');
        b=b(end).filename;
        fprintf(fid1,['%s' drm],b);
        if SLIDEVERSION<1051019
            peakZ(1).fileZ(j).date=hdrinfo(j).date;
        end
        try
            [dayoweek, dayt] = strtok(peakZ(1).fileZ(j).date,',');

            dateloc = findstr(dayt,'-');
            dayt = dayt(dateloc(1)-2:dateloc(end));
            time = voctimecode(char(peakZ(1).fileZ(j).name));
            dayt =  strcat(dayt,time);
        catch
            dayoweek='';
            dayt='';
        end
        fprintf(fid1,['%s' drm],dayoweek);
        fprintf(fid1,['%s' drm], dayt);

        % write out the data into its correct location
        pkArez=0;
        for k = 2:length(peakZ);
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(2));
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(1));
            fprintf(fid1,['%12.4f' drm],gArea(peakZ,k,j)/(sqrt(2*pi)*peakZ(k).fileZ(j).gParams(1)));
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(4));
            fprintf(fid1,['%12.4f' drm],gArea(peakZ,k,j));
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).area);
            fprintf(fid1,['%12.4f' drm],peakZ(k).fileZ(j).gParams(7));
            % calcuate the TOTAL column
        end
        fprintf(fid1,'\n');
    end
    fclose(fid1);
    cd(saveDir);
catch
    warning('Error saving output files. No Output was saved');
end
disp('Finished Writing Files');

function area=gArea(peakZ,k,j)
% calculate the gaussian area for a given peak and alter this number if the
% peak is leaning
leanConst=(1/2+1/2*1/peakZ(k).fileZ(j).gParams(4));
area=leanConst*peakZ(k).fileZ(j).gParams(3)*sqrt(2*pi)*peakZ(k).fileZ(j).gParams(1);


function [peakZ]=slider(peakZ,xd,flecnt,pkstr,cPeak,peakFollowName,pkName,grp)

jkt=peakZ(1).fileZ(1).x;
x1=xd(1);
x2=xd(2);
% find what x-value the user probably clicked
s1=find(abs(peakZ(1).fileZ(flecnt).x-x1)==min(abs(peakZ(1).fileZ(flecnt).x-x1)));
s2=find(abs(peakZ(1).fileZ(flecnt).x-x2)==min(abs(peakZ(1).fileZ(flecnt).x-x2)));
peakZ(end).refData=[peakZ(1).fileZ(flecnt).y(s1:s2)];
s3=s2-s1;
fcknug=[];
peakZ(end+1)=peakZ(end);
peakZ(end).fileZ(flecnt).pkbnd=[x1 x2];
peakZ(end).pkName=pkName;
peakZ(end).peakRoot=[x1 x2];
peakZ(end).peakStr=pkstr;
peakZ(end).peakId=cPeak;

peakZ(end).peakMisc.fitFun='@gaussfun';
peakZ(end).peakMisc.peakFollow=peakFollowName;
peakZ(end).peakGroup=grp;
for npn=[2:length(peakZ)] % this sets to follower distance
    if strcmp(peakFollowName,peakZ(npn).pkName);
        peakZ(npn).peakRoot=peakZ(npn).fileZ(flecnt).pkbnd;
    end
end
peakZ=slideSearch(peakZ,xd,flecnt)



function [peakZ]=slideSearch(peakZ,xd,flecnt)
global baroSTOP
global lstpeak;
global linehandles;
global SlideFigure;
baroSTOP=0;
hold on;
xIn=xd;
n=flecnt+1;
while n<=length(peakZ(1).fileZ)
    [peakZ xtemp]=doPeak(peakZ,xIn,length(peakZ),n);
    [peakZ xtemp]=doPeak(peakZ,xIn,length(peakZ),n);
    if baroSTOP==1
        lstpeak=n;
        set(linehandles,'LineStyle','-');
        set(linehandles,'Marker','none');
        set(linehandles(lstpeak),'Marker','.');
        if SlideFigure>-1
            figure(SlideFigure);
            title(['Chromatogram #' num2str(lstpeak)]);
        end
        
        uiwait(msgbox('Select Chromatogram where the peak begins to go off track using the Arrow Keys on the keyboard and then click ok'));
        n=lstpeak;      
        uiwait(msgbox('Select Peak Using Mouse'));
        [xIn yIn]=ginput(2);
        baroSTOP=0;
    else
        xIn=xtemp;
        n=n+1;
    end
    
end
xIn=xd;
n=flecnt;
while n>=1
    [peakZ xtemp]=doPeak(peakZ,xIn,length(peakZ),n);
    if baroSTOP==1
        lstpeak=n;
        set(linehandles,'LineStyle','-');
        set(linehandles,'Marker','none');
        set(linehandles(lstpeak),'Marker','.');
        if SlideFigure>-1
            figure(SlideFigure);
            title(['Chromatogram #' num2str(lstpeak)]);
        end
        
        uiwait(msgbox('Select Chromatogram where the peak begins to go off track using the Arrow Keys on the keyboard and then click ok'));
        n=lstpeak;      
        uiwait(msgbox('Select Peak Using Mouse'));
        [xIn yIn]=ginput(2);
        baroSTOP=0;
    else
        xIn=xtemp;
        n=n-1;
    end
    
end


function [peakZ,xTemp]=doPeak(peakZ,xd,peakNum,n)
% parameters for Dr. O's AntiShifting
% BAROBARO
TOLSNR=80; % gaussing must be a at least 40 snr in order to even attempt to shift base
TOLHEIGHT=2; % 600% higher than the noise
TOLDRIFT=3; % 3 times larger than the peak half-width

% find the shift of the peaks
colorZ='k';
cPeak=peakZ(peakNum).peakId;
pkstr=peakZ(peakNum).peakStr;
s3=length(peakZ(peakNum).refData)-1;
peakZ(peakNum).fileZ(n).x=[];
peakZ(peakNum).fileZ(n).y=[];
dumAss=0;
disp(['Current file:' cell2mat(peakZ(1).fileZ(n).name) '; Peak: ' num2str(sum(peakZ(peakNum).peakRoot)/2) '.pks']);
peaky=peakZ(peakNum).peakMisc.peakFollow;
if ~isempty(peaky)
    cshft=0;
    cn=0;
    for k=[2:length(peakZ)]
        if strcmp(peakZ(k).pkName,peaky)
            px1=max(peakZ(k).peakRoot); % the seed x1 coordinate of the leader peak
            %px1=max(find(abs(peakZ(1).fileZ(n).x-px1)==min(abs(peakZ(1).fileZ(n).x-px1))));
            px2=max(peakZ(k).fileZ(n).pkbnd); % the current x1 coordinate of the leader peak
            %px2=max(find(abs(peakZ(1).fileZ(n).x-px2)==min(abs(peakZ(1).fileZ(n).x-px2))));
            pxx1=min(peakZ(peakNum).peakRoot); % the seed x1 coordinate of the follower peak
            %pxx1=max(find(abs(peakZ(1).fileZ(n).x-pxx1)==min(abs(peakZ(1).fileZ(n).x-pxx1))));
            % newX is the
            cshft=pxx1+(px2-px1); % starting retention time
            pxx1;
            iShift=px2-px1;
            k;
            % disp(['I am ' peaky ' and I am a ' peakZ(k).pkName]);
            % try another method where you just try to make sure that the
            % distance between the peaks is the same
            % px1=min(peakZ(k).peakRoot);
            % px2=max(peakZ(k).fileZ(n).pkbnd);
            % pxx1=min(peakZ(peakNum).peakRoot);
            % newX is the root location of x plus the change from the
            % leader graph
            % cshft=pxx1+(px2-px1);
            % iShift=px2-px1
            cn=cn+1;
        end
    end
    try
    if cn>0
        % shft=cshft;
        x1=cshft;
        ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
        % ss1=cshft;
        % x1=peakZ(1).fileZ(n).x(ss1);
        ss2=ss1+s3;
        x2=peakZ(1).fileZ(n).x(ss2);
        ssnr=0.0;
        shft=0;
        cng=[ss1:ss2];
    else
        dumAss=1; % no leader found
        warning(['Leader peak ' peaky ' is AWOL']);
    end
    catch
         dumAss=1; % no leader found
        warning(['Leader peak ' peaky ' is AWOL']);
    end
end

if (isempty(peaky) | dumAss)
    x1=min(xd);
    x2=max(xd);


    % locate the x boundary coordinates (time or datapt#) in the current file
    ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
    ss2=max(find(abs(peakZ(1).fileZ(n).x-x2)==min(abs(peakZ(1).fileZ(n).x-x2))));

    while ss2~=ss1+s3
        while ss2<ss1+s3
            ss1=ss1-round(rand*2);
            ss2=ss2+round(rand*2);
        end
        while ss2>ss1+s3
            ss1=ss1+round(rand*2);
            ss2=ss2-round(rand*2);
        end
    end
    % create the initial search range
    cng=[ss1:ss2];
    x1=peakZ(1).fileZ(n).x(ss1);
    x2=peakZ(1).fileZ(n).x(ss2);
    % determine current graph's collision boundaries
    leftBound=min(peakZ(1).fileZ(n).x);
    rightBound=max(peakZ(1).fileZ(n).x);
    for bjt=[2:peakNum-1]
        dPeak=peakZ(bjt).fileZ(n).pkbnd;
        if (x1+x2)/2>sum(dPeak)/2 & ~strcmp(peakZ(bjt).peakMisc.peakFollow,peakZ(peakNum).pkName) % & isempty(peakZ(bjt).peakMisc.peakFollow) % only prevents collisions between leader peaks
            leftBound=max(dPeak);
        end
    end
    for bjt=[peakNum-1:-1:2]
        dPeak=peakZ(bjt).fileZ(n).pkbnd;
        if (x1+x2)/2<sum(dPeak)/2 & ~strcmp(peakZ(bjt).peakMisc.peakFollow,peakZ(peakNum).pkName) % & isempty(peakZ(bjt).peakMisc.peakFollow) % only prevents collisions between leader peaks
            rightBound=min(dPeak);
        end
    end
    leftBound=find(abs(peakZ(1).fileZ(n).x-leftBound)==min(abs(peakZ(1).fileZ(n).x-leftBound)));
    rightBound=find(abs(peakZ(1).fileZ(n).x-rightBound)==min(abs(peakZ(1).fileZ(n).x-rightBound)));
    if pkstr>0
    [shft,boobless,ssnr]=jaque(peakZ(peakNum).refData,peakZ(1).fileZ(n).y,cng,pkstr,[leftBound rightBound]);
    else
        shft=0;
        boobless=0;
        ssnr=0;
    end
    if ssnr<10
        colorZ='b';
    end
    if ssnr<0
        colorZ='r';
    end
    if ssnr<0
        disp('Slide method yielded exceptionally poor results, disabling');
        shft=0;
    end
end

if length(peakZ(1).fileZ(n).x)<(ss1+s3+shft) % redonkulously small data set
    warning(['Seriousy short data in ' cell2mat(peakZ(1).fileZ(n).name)]);
    peakZ(peakNum).fileZ(n).pkbnd=[1 3];
    peakZ(peakNum).fileZ(n).pkrange=[1 2 3];
    peakZ(peakNum).fileZ(n).area=-1;
    peakZ(peakNum).fileZ(n).gParams=[0 1 1 1 0 0 0];
else % actually do stuff
    % plot the new peak boundaries
    c1=[peakZ(1).fileZ(n).x(ss1+shft)];
    c2=[n*peakZ(1).plotsep+peakZ(1).fileZ(n).y(ss1+shft)];
    plot(c1,c2,[colorZ 'o'],'MarkerSize',2)
    drawnow;
    % junkX1=[junkX1 c1];
    % junkY1=[junkY1 c2];
    % plot the new peak boundaries
    %  c1=[peakZ(1).fileZ(n).x(ss2+shft)];
    %  c2=[n*peakZ(1).plotsep+peakZ(1).fileZ(n).y(ss2+shft)];

    %   plot(c1,c2,[colorZ '+'],'MarkerSize',2)
    %junkX2=[junkX2 c1];
    %junkY2=[junkY2 c2];
    % update the screen
    % pause(.1)
    % set x1 and x2 to the new values
    x1=peakZ(1).fileZ(n).x(ss1+shft);
    x2=peakZ(1).fileZ(n).x(ss2+shft);
    xTemp=[x1 x2];
    % store values and integrate
    peakZ(peakNum).fileZ(n).pkbnd=[x1 x2];
    peakZ(peakNum).fileZ(n).pkrange=[cng+shft];
    peakZ(peakNum).fileZ(n).area=pkInt(peakZ(1).fileZ(n),cng+shft);

    % gaussian addition

    xdata=peakZ(1).fileZ(n).x(ss1+shft:ss1+s3+shft);
    ydata=peakZ(1).fileZ(n).y(ss1+shft:ss1+s3+shft);

    %off scale points delete code
    ofScal=find(ydata>1e6);
    ydata(ofScal)=[];
    xdata(ofScal)=[];

    if length(xdata)<5
        warning(['Significant amount of data is off-scale in file ' cell2mat(peakZ(1).fileZ(n).name)]);
        peakZ(peakNum).fileZ(n).pkbnd=[1 3];
        peakZ(peakNum).fileZ(n).pkrange=[1 2 3];
        peakZ(peakNum).fileZ(n).area=-1;
        peakZ(peakNum).fileZ(n).gParams=[0 1 1 1 0 0 0];
    else
        try
            ydata=smooth(ydata,5,'sgolay');
        catch
            disp('Can''t Smooth');
        end
        lb=[-inf,min(xdata)+range(xdata)/13,.75,1-1e-3,-inf,-inf];
        ub=[ inf,max(xdata)-range(xdata)/13,range(xdata)/2,1+1e-3,inf,inf];

        if isempty(cPeak)
            cPeak=peakZ(peakNum).peakId;
        end

        vecslope=mean(diff(smooth(ydata,5)))*1/mean(diff(xdata));
        lb(5)=min([1.3*vecslope,1/1.3*vecslope]);
        ub(5)=max([1.3*vecslope,1/1.3*vecslope]);
        cydat=(ydata-ydata(1))-vecslope*(xdata-xdata(1));
        lb(end)=min(ydata)-vecslope*max(xdata);
        ub(end)=max(ydata)-vecslope*min(xdata);
        [pkheight,pkcenter]=max(cydat);
        pkcenter=xdata(pkcenter(1));
        pkwid=range(xdata)/6;
        distfac=1;
        if ~isempty(cPeak)
            try
                pkwid=cPeak.pkwid;
                % Peak center isnt a good variable
                %  lb(2)=cPeak.pkcenter-cPeak.pkvar;
                %  ub(2)=cPeak.pkcenter+cPeak.pkvar;
                lb(3)=max([cPeak.pkwid-cPeak.widvar,.75]);
                ub(3)=cPeak.pkwid+cPeak.widvar;
            catch
                disp('No PeakWidth');
            end
            try
                distfac=cPeak.df;
                lb(4)=max([cPeak.df-cPeak.dfvar,1e-5]);
                ub(4)=cPeak.df+cPeak.dfvar;
                lb(5)=min([1.1*vecslope,.1*vecslope]); % give a 10% tolerance for the slope
                ub(5)=max([1.1*vecslope,.1*vecslope]);
            catch
                disp('No Distortion')
            end


        end
        
        
        OPTIONS=optimset('MaxFunEvals',90000,'MaxIter',1200,'Display','off');
        gParamsz=[pkheight(1),pkcenter,pkwid,distfac,vecslope,ydata(1)-vecslope*xdata(1)];
        if prod(double((ub-lb)>0))==1
            [gParams,bulldog,resid]=lsqcurvefit(@gaussfun,gParamsz(1:6),xdata,ydata,lb,ub,OPTIONS);
        else
            gParams=gParamsz;
            gParams(1)=0;
        end
        if sum(double(gParams>ub)) | sum(double(gParams<lb)) | gParams(2)<min(xdata) | gParams(2)>max(xdata) | 1.2*std(ydata)>gParams(1)
            gParams(1)=0;
        end
        signal=gaussfun(gParams(1:6),xdata);
        isignal=gaussfun(gParamsz(1:6),xdata);
        isolsignal=gaussfun([gParams(1:4) 0 0],xdata);
        noise=abs(ydata-signal);
        s2n1=(isolsignal.^2)./(noise.^2);
        s2n2=sqrt(sum(s2n1));
        s2n3=s2n2/length(noise);
        s2n=20*log(s2n3);
        disp(['SNR #' num2str(n) ' {Shift, Gaussian}: {' num2str(round(ssnr*10)/10) ' , ' num2str(round(s2n*10)/10) '}']);
        %figure;plot(xdata,ydata,'r+');hold on;plot(xdata,signal,'b-');plot(xdata,isignal,'g-');
        %line([gParams(2) gParams(2)],[min(ydata) max(ydata)]);
        %line([gParams(2)-gParams(3) gParams(2)],[min(ydata) max(ydata)]);
        %line([gParams(2)+gParams(3) gParams(2)],[min(ydata) max(ydata)]);
        %pause(1);hold off;close;
        % code to plot the gaussian on the real graph
        if peakZ(peakNum).peakGroup<1
            disp('Plotting Signal');
            plot(xdata,signal+n*peakZ(1).plotsep,'k-');
            plot(xdata,isignal+n*peakZ(1).plotsep,'r.');
            plot(xdata,ydata+n*peakZ(1).plotsep,'b.');
            drawnow;
        end
        peakZ(peakNum).fileZ(n).gParams=[gParams s2n];
        kn1=max(gParams(1)/(sum(noise)/length(noise)));
        kn2=max(abs(((x1+x2)/2-gParams(2))/gParams(3)));
        disp(['Slidin Check: H2N ' num2str(kn1) ' DRFT-' num2str(kn2)]);
        disp(['Slidin Check: DD ' num2str(abs((x1+x2)/2-gParams(2)))]);
        pFixed=0;
        if (isempty(peaky) | dumAss) % the peak cant be fixed it its just following orders
            if s2n>TOLSNR & kn1>TOLHEIGHT % make sure peak is big enough and nice enough
                if kn2>TOLDRIFT % make sure the peak has drifted enough from the center of the datapts
                    % redo x1,x2,refdata
                    warning('Peak Appears to be drifting away from the center, correcting');
                    x1=gParams(2)-gParams(3)*6;
                    x2=gParams(2)+gParams(3)*6;
                    ss1=max(find(abs(peakZ(1).fileZ(n).x-x1)==min(abs(peakZ(1).fileZ(n).x-x1))));
                    ss2=max(find(abs(peakZ(1).fileZ(n).x-x2)==min(abs(peakZ(1).fileZ(n).x-x2))));

                    peakZ(peakNum).refData=peakZ(1).fileZ(n).x(ss1:ss2);
                    peakZ(peakNum).fileZ(n).pkbnd=[x1 x2];
                    peakZ(peakNum).fileZ(n).pkrange=[ss1:ss2];
                    peakZ(peakNum).fileZ(n).area=pkInt(peakZ(1).fileZ(n),[ss1:ss2]);
                    [peakZ,xTemp]=doPeak(peakZ,[x1 x2],peakNum,n);
                    pFixed=1;
                end
            end
        end
        if pFixed==0
            c1=[peakZ(1).fileZ(n).x(ss2+shft)];
            c2=[n*peakZ(1).plotsep+peakZ(1).fileZ(n).y(ss2+shft)];

            plot(c1,c2,[colorZ '+'],'MarkerSize',2)
        end

    end
end
try
    % end gaussian
catch
    warning(['Something just fischy about the data ' cell2mat(peakZ(1).fileZ(n).name)]);
    peakZ(peakNum).fileZ(n).pkbnd=[1 3];
    peakZ(peakNum).fileZ(n).pkrange=[1 2 3];
    peakZ(peakNum).fileZ(n).area=-1;
    peakZ(peakNum).fileZ(n).gParams=[0 1 1 0 0 0];
end

function area=pkInt(cpeak,range)
% code to integrate using the trapezoidal method the peak and subtract the baseline
x=cpeak.x(range);
y=cpeak.y(range);
m=(y(end)-y(1))/(x(end)-x(1));
baseline=m*(x-x(1))+y(1);
% area=rawInt(x,y,baseline);
area=trapz(x,detrend(y)); % new area routine for traps that uses a least squares line fit

function area=rawInt(x,y,baseline);
if isnan(baseline)
    % only occurs when the peak consists of one point, but should a
    % possiblity nonetheless
    area=0;
else
    if prod(double(size(y)==size(baseline)))
        area=trapz(x,y-baseline);
    else
        area=trapz(x,y-baseline');
    end
end
% allow negative area
%if area<0
%    area=0;
%end

function [shft,jed,snr]=jaque(refpeak,cgraph,rng,ns,boundZ)
% code to make sure it isnt shifting the peak outside of the data
% boundaries
nsdown=ns;
nsup=ns;
if (rng(1)-nsdown)<min(boundZ)
    nsdown=abs(min(boundZ)-(rng(1)));
    % the downswing is equal to the difference between the minimum value
    % and the beginning
end
if (rng(end)+nsup)>max(boundZ)
    nsup=max(boundZ)-rng(end)-1;
    % the upswing is equal to the maximum value minus the end of the range
    % -1
end
n=-nsdown;
refpeak=refpeak-refpeak(1);
j={};
%figure
while n<=nsup
    cpeak=cgraph(rng+n);
    cpeak=cpeak-cpeak(1);
    % mfact is the factor you can multiply the current peaks area by to
    % match that of the reference peaks
    mfact=max(refpeak)/max(cpeak);
    % mfact is limited to .25 to 4 or else noise can get amplified to look
    % like a peak
    mmax=8;
    if ns>1000
        mmax=12;
    end
    if mfact<inv(mmax)
        mfact=inv(mmax);
    end
    if mfact>mmax
        mfact=mmax;
    end
    cpeak=mfact*cpeak;
    % find the difference between the reference peak and the current peak
    j(end+1).jval=sqrt(sum((refpeak-cpeak).^2)/length(cpeak));
    j(end).offset=n;


    n=n+1;
end
if isempty(j)
    shft=0;
    resid=1e9;
    jed.jval=1e10;
    jed.offset=0;
else
    [alber shft]=min([j.jval]);
    resid=j(shft).jval;
    shft=j(shft).offset;
    jed=j;
end
if resid==0
    resid=0.1;
end
snr=20*log(sqrt(sum(refpeak.^2)/length(refpeak))/resid);
% disp(['Shift S/N: ' num2str(snr)]);


function pk2 = pkfind(x,p2)
%PKFind
%
%  pk = pkfind(x,p)   peak hunt down using peak half window width of p.
%
% Returns PK, a list of values of indices of the peak max
%
x=x(:);
p2=floor(p2);
p = 2*p2 + 1;          % full window width
[n,m] = size(x);
z = zeros(p2,m);
j = toeplitz(p:-1:1,p:n);    % shift operator
%
y = zeros(p,(n-p+1)*m);
y(:) = x(j,:);
ma = max(y);                 % find maximum in window
%
pk = [z ; reshape(ma,n-p+1,m) ; z]; % add missing edge elements
pk = pk == x;                 % find matching elements
%%
pk2=find(pk);

function loadMaster
% This function loads a master list of peak information from an easily
% editable excel file the information includes the peaks name, the peaks
% center and a plethora of other information
global MasterPeak
MasterPeak={};
if length(dir([pwd '\MasterRetentionList.xls']))>0
    [pkdat,names]=xlsread([pwd '\MasterRetentionList.xls']);
else
    [pkdat,names]=xlsread([matlabroot '\work\slidetools\MasterRetentionList.xls']);
end
pknm=names([2:end],1)
for i=[1:length(pknm)]
    MasterPeak(end+1).name=cell2mat(pknm(i));
    MasterPeak(end).pkcenter=pkdat(i,1);
    MasterPeak(end).pkvar=pkdat(i,2);
    MasterPeak(end).pkwid=pkdat(i,3);
    MasterPeak(end).widvar=pkdat(i,4);
    MasterPeak(end).visible=pkdat(i,5);
    MasterPeak(end).df=pkdat(i,6);
    MasterPeak(end).dfvar=pkdat(i,7);
end
function drange=range(data)
drange=max(data)-min(data);

function [pkName,cPeak]=identifyPeak(xd)
global MasterPeak
PKCNT=4;
fakePeakWidth=range(xd)/6; % a rough guess of the peaks half width
peakCenter=sum(xd)/2; % the peaks center
% first step find the closest to the peak center
dArr=abs(peakCenter-[MasterPeak.pkcenter]);
[junk,twist]=sort(dArr);
%likely=twist([1:PKCNT]); % ignore the PKCNT
likely=twist;
% Formula for figuring out how likely you have a match

%Center Portion
cArr=([MasterPeak(likely).pkvar]-abs([MasterPeak(likely).pkcenter]-peakCenter))./[MasterPeak(likely).pkvar];
cFactor=.99+(double(cArr<0).*cArr);
cFactor=double(cFactor>.1).*cFactor+.01;

%Width Portion
wArr=([MasterPeak(likely).widvar]-abs([MasterPeak(likely).pkwid]-fakePeakWidth))./[MasterPeak(likely).widvar];
wFactor=.99+(double(wArr<0).*wArr);
wFactor=double(wFactor>.1).*wFactor+.01;
%Likelyhood Peak actually appears
pFactor=[MasterPeak(likely).visible];

rankz=(5*cFactor)/5 % +pFactor)/7;
[junk,twist]=sort(1./rankz);
% hugeWaste;
pkz={};
for ij=[1:length(twist)]
    centVar='';
    mPeak=MasterPeak(likely(twist(ij)));
    if mPeak.pkvar>0
        centVar=['; ' num2str(mPeak.pkcenter-mPeak.pkvar) '-' num2str(mPeak.pkcenter+mPeak.pkvar)];
    end

    cmName=[mPeak.name centVar ' - Likelyhood: ' num2str(1/junk(ij)*100) '%'];
    pkz(end+1)={cmName};
end
pkz(end+1)={'Other...'};
pkSel=menu('Identify what peak this is',pkz);
if pkSel==length(pkz)
    cRoot=peakCenter;
    cN=zpad(cRoot,4);
    pkName=[cN '.pks'];
    cPeak={};
else
    cPeak=MasterPeak(likely(twist(pkSel)));
    pkName=cPeak.name;
end

function f=gaussfun(A,x)
f=0;
for ii=0:(round((length(A)-2)/4)-1)
    f =f + (x>=A(ii*4+2)).*A(ii*4+1).*exp(-0.5.*((x-A(ii*4+2))/A(ii*4+3)).^2);
    f =f + (x<A(ii*4+2)).*A(ii*4+1).*exp(-0.5.*((x-A(ii*4+2))/(A(ii*4+3)/A(ii*4+4))).^2);
end
f=f+A(end-1)*x+A(end);


function cN=zpad(cRoot,digz)
cN=num2str(round(cRoot));
while length(cN)<digz
    cN=['0' cN];
end


function [peakZ]=doPeakGroup(peakZ,n,grams)
% peakZ is the massive peak structure
% n is the group to operate on
% this function performs the loving task of taking a group of peaks and
% performing a multipeak fitting for each graph
% internal consistency j represents a peak, k represents a chromatogram
gPeaks=[];
gPeakLZ=[];
for j=[2:length(peakZ)]
    if peakZ(j).peakGroup==n
        gPeaks(end+1)=j; % make a list of all the peaks in the given group
        gPeakLZ(end+1)=sum(peakZ(j).fileZ(1).pkbnd)/2; % get the centerlocation from the first chromatogram for ordering purposes
    end
end
[adamDuritzIsFreakingAwesome reorderz]=sort(gPeakLZ)
gPeaks=gPeaks(reorderz); % make sure the peaks are in chronological order for the fitting
for k=[grams]
    % this is the code it will run on each chromatogram
    x=peakZ(1).fileZ(k).x;
    y=peakZ(1).fileZ(k).y;

    % make all the bounds into a new data set
    cBounds=[];

    for j=gPeaks
        cBounds=[cBounds peakZ(j).fileZ(k).pkbnd];
    end
    % translate these into point number (so i can get y-values)
    cPts=[];
    for ii=[1:length(cBounds)]
        cPts(end+1)=max(find(abs(x-cBounds(ii))==min(abs(x-cBounds(ii)))));
    end
    % crop data set to just the area of interest
    x=x(min(cPts):max(cPts));
    y=y(min(cPts):max(cPts));
    cPts=cPts-min(cPts); % make sure the points are shifted so the still correspond to the local x,y
    % find the first two parameters based on the first and last points of
    % the data set
    vecslope=mean(diff(smooth(y,5)))*1/mean(diff(x)); % slope of the boundaries
    constTerm=y(1)-x(1)*vecslope; % constant term of the boundaries

    % create the seed and boundary values
    lb=[];
    ub=[];
    gParam=[];
    for j=gPeaks
        cPeak=peakZ(j).fileZ(k);
        % peak height
        lb(end+1)=[0];
        % ub(end+1)=[+1.1*abs(cPeak.gParams(1))];
        ub(end+1)=[(max(y)-min(y))];

        gParam(end+1)=[cPeak.gParams(1)];
        % peak center
        % peak center
        gParam(end+1)=[cPeak.gParams(2)]; % pkcenter
        gParam(end+1)=[cPeak.gParams(3)]; %pk halfwidth
        if ~isempty(peakZ(j).peakId)
            %             if peakZ(j).peakId.pkvar>0 %pk center
            %                 lb(end+1)=max([peakZ(j).peakId.pkcenter-peakZ(j).peakId.pkvar,cPeak.pkbnd(1)]);
            %                 ub(end+1)=min([peakZ(j).peakId.pkcenter+peakZ(j).peakId.pkvar,cPeak.pkbnd(2)]);
            %             else
            lb(end+1)=[cPeak.pkbnd(1)];
            ub(end+1)=[cPeak.pkbnd(2)];
            %             end
            lb(end+1)=max([peakZ(j).peakId.pkwid-peakZ(j).peakId.widvar,.5]); %pk halfwidth
            ub(end+1)=peakZ(j).peakId.pkwid+peakZ(j).peakId.widvar;

        else
            lb(end+1)=[cPeak.pkbnd(1)];
            ub(end+1)=[cPeak.pkbnd(2)];
            lb(end+1)=[1.5];
            ub(end+1)=[range(cPeak.pkbnd)/2.5];
        end
        try
            distfac=peakZ(j).peakId.df;
            dvar=peakZ(j).peakId.dfvar;

        catch
            distfac=1;
            dvar=1e-4;
        end
        lb(end+1)=max([distfac-dvar,1e-5]);
        ub(end+1)=distfac+dvar;
        gParam(end+1)=cPeak.gParams(4);
    end

    % last parameters
    % slope
    lb(end+1)=min([1.25*vecslope,.75*vecslope]);
    ub(end+1)=max([1.25*vecslope,.75*vecslope]);
    isolParam=length(gParam);
    gParam(end+1)=vecslope;
    % offset
    lb(end+1)=min(y)-vecslope*max(x);
    ub(end+1)=max(y)-vecslope*min(x);
    %lb(end+1)=[-inf];
    %ub(end+1)=[inf];
    gParam(end+1)=constTerm;
    % now run ze wonderfal fit (cross your fingers)
    OPTIONS=optimset('MaxFunEvals',90000*length(gPeaks),'MaxIter',1200*length(gPeaks),'Display','off');
    eFlag=0;
    if prod(double((ub-lb)>0))==1 % check that lb and ub make sense
        [gParams,bulldog,resid,eFlag]=lsqcurvefit(@gaussfun,gParam,x,y,lb,ub,OPTIONS);
    else
        warning('Bounds are Problematic');
        gParams=gParam;
    end
    if eFlag==-2
        warning('Bounds are Inconsistent');
    end
    if sum(gParams>ub)>0
        warning('Output Exceeds Upperbounds');
    end
    if sum(gParams<lb)>0
        warning('Output is less than Lowerbounds');
    end
    signal=gaussfun(gParams,x);
    isignal=gaussfun(gParam,x);
    isolsignal=gaussfun([gParams(1:isolParam) 0 0],x);
    noise=abs(y-signal);
    s2n1=(isolsignal.^2)./(noise.^2);
    s2n2=sqrt(sum(s2n1));
    s2n3=s2n2/length(noise);
    s2n=20*log(s2n3);



   for j=1:length(gPeaks)
        peakZ(gPeaks(j)).fileZ(k).gParams=[gParams([(j-1)*4+1:(j-1)*4+4]) gParams(end-1) gParams(end) s2n];
    end
    hold on;
    plot(x,signal+k*peakZ(1).plotsep,'k-');
    drawnow;
    % debugging code
    h=figure;
    plot(x,y,'r.',x,signal,'b-',x,isignal,'g-');
    title(['Peak: ' peakZ(gPeaks(j)).pkName ' Chromatogram #: ' num2str(k)]);
    pause(2);
    close(h);
end


function int2jpg(peakZ)
% int2jpg performs the same function as slidemovie,

cpr=cell2mat(inputdlg('Enter directory name..','User Input Required'))
eval(['mkdir ' cpr]);
cd(cpr);
grps=max([peakZ.peakGroup]);
masList=[1:length(peakZ)] % all the peaks
% now make sure the group peaks are only listed once
delList=[1];
for np=[1:grps]
    a=find([peakZ.peakGroup]==np);
    if ~isempty(a)
        a(1)=[]; % leave the first one
        delList=[delList a];
    end
end
masList(delList)=[];
str={};
for ik=masList
    if peakZ(ik).peakGroup>0
        str(end+1)={['Group ' num2str(peakZ(ik).peakGroup) ':' peakZ(ik).pkName]};
    else
        str(end+1)={peakZ(ik).pkName};
    end
end
[s,v] = listdlg('PromptString','Select Peaks to Save as JPG:','ListString',str);
if v==1
    masList=masList(s);
end
for k=masList
    for j=[1:length(peakZ(1).fileZ)]
        h=figure;
        cPeak=peakZ(k).fileZ(j);
        hold on;
        sPoints=[];
        ePoints=[];
        if peakZ(k).peakGroup<1
            sC='P';
            cPeak=peakZ(k).fileZ(j);
            x1=min(cPeak.pkbnd);
            x2=max(cPeak.pkbnd);
            ss1=find(abs(peakZ(1).fileZ(j).x-x1)==min(abs(peakZ(1).fileZ(j).x-x1)));
            ss2=find(abs(peakZ(1).fileZ(j).x-x2)==min(abs(peakZ(1).fileZ(j).x-x2)));
            sPoints(end+1)=[ss1(1)];
            ePoints(end+1)=[ss2(end)];
        else
            sC='G';
            gpl=find([peakZ.peakGroup]==peakZ(k).peakGroup);
            bnds=[];
            for jk=gpl
                cPeak=peakZ(jk).fileZ(j);
                bnds=[bnds cPeak.pkbnd];
                x1=min(bnds(end-1:end));
                x2=max(bnds(end-1:end));
                ss1=find(abs(peakZ(1).fileZ(j).x-x1)==min(abs(peakZ(1).fileZ(j).x-x1)));
                ss2=find(abs(peakZ(1).fileZ(j).x-x2)==min(abs(peakZ(1).fileZ(j).x-x2)));
                sPoints(end+1)=[ss1(1)];
                ePoints(end+1)=[ss2(end)];
            end
            x1=min(bnds)-50;
            x2=max(bnds)+50;
        end
        ss1=find(abs(peakZ(1).fileZ(j).x-x1)==min(abs(peakZ(1).fileZ(j).x-x1)));
        ss2=find(abs(peakZ(1).fileZ(j).x-x2)==min(abs(peakZ(1).fileZ(j).x-x2)));
        ss1=ss1(1);
        ss2=ss2(1);
        xdata=peakZ(1).fileZ(j).x(ss1:ss2);
        ydata=peakZ(1).fileZ(j).y(ss1:ss2);

        if peakZ(k).peakGroup<1
            gUnit=peakZ(k).fileZ(j).gParams(1:6);
        else
            gUnit=[];
            gpl=find([peakZ.peakGroup]==peakZ(k).peakGroup);
            for jk=gpl
                cPeak=peakZ(jk).fileZ(j);
                gUnit=[gUnit cPeak.gParams(1:4)];
                gdata=gaussfun(cPeak.gParams(1:6),xdata);

                plot(xdata,gdata,'c-'); % plot the peaks individually
            end

            gUnit=[gUnit cPeak.gParams(5:6)]

        end
        gdata=gaussfun(gUnit,xdata);

        sldata=gaussfun([0 0 0 0 cPeak.gParams(5:6)],xdata);

        plot(xdata,ydata,'r+');

        plot(xdata,gdata,'k-');
        plot(xdata,sldata,'g.');
        plot(peakZ(1).fileZ(j).x(sPoints),peakZ(1).fileZ(j).y(sPoints),'ko','MarkerSize',15);
        plot(peakZ(1).fileZ(j).x(ePoints),peakZ(1).fileZ(j).y(ePoints),'k+','MarkerSize',15);
        fname=[str2mat(peakZ(1).fileZ(j).name) '-' sC num2str((x1+x2)/2) 'C' num2str(j)];
        title([fname  '.pks']);
        pbj=find(fname=='\');
        saveas(h,[fname(pbj(end)+1:end) '.jpg'],'jpg');
        close(h);
    end
end
eval('cd ..');


function moldyCode
% old code im too attached to too delete
if getpklist==3;
    linehandles=slidestack(peakZ)
    uiwait(msgbox('Select Graph Using Arrow Keys'))
    pks=inputdlg('About How Many Peaks Should be Found? (Overestimate if nessecary)');
    pks=str2num(cell2mat(pks));
    if 1<pks<30
        % user gave a valid number
    else
        % user is messing with me
        msgbox('Go away, your only getting 16 peaks')
    end
    [xd,flecnt,pkstr]=slidepick;
    x1=xd(1);
    x2=xd(2);
    % find what x-value the user probably clicked
    s1=find(abs(peakZ(1).fileZ(flecnt).x-x1)==min(abs(peakZ(1).fileZ(flecnt).x-x1)));
    s2=find(abs(peakZ(1).fileZ(flecnt).x-x2)==min(abs(peakZ(1).fileZ(flecnt).x-x2)));
    s3=s2-s1;
    refpeak=peakZ(1).fileZ(flecnt).y(s1:s2);
    pause(.2);
    cpks=0;
    wdth=100;
    jjkz=0;
    while abs(cpks-pks)>2 & jjkz<15
        fpks=pkfind(peakZ(1).fileZ(flecnt).y,wdth);
        cpks=length(fpks);
        if cpks>pks
            wdth=wdth*1.25;
        else
            wdth=wdth*.75;
        end
        jjkz=jjkz+1;
    end
    plot(peakZ(1).fileZ(flecnt).x(fpks),peakZ(1).fileZ(flecnt).y(fpks)+flecnt*peakZ(1).plotsep,'ro');
    refpeak=refpeak-refpeak(1);
    % width-range 50-1000 pts
    swid=25;
    ewid=500;
    kval=[swid:2:ewid];
    for k=1:length(fpks)
        bmat=[];
        for kwid=kval
            cng=[fpks(k)-kwid;fpks(k)+kwid];
            if cng(1)<1
                cng(1)=1;
            end
            if cng(2)>length(peakZ(1).fileZ(flecnt).y)
                cng(2)=length(peakZ(1).fileZ(flecnt).y);
            end
            cdat=peakZ(1).fileZ(flecnt).y(cng(1):cng(2));
            rdat=interpft(refpeak,range(cng)+1);
            cdat=cdat-cdat(1);
            mfact=max(rdat)/max(cdat);
            if mfact>10
                mfact=10;
            elseif inv(mfact)>10
                mfact=inv(10);
            end
            cdat=cdat.*mfact;
            bmat=[bmat sum(abs(cdat-rdat))/length(cdat)];
        end
        [apple,orange]=min(bmat);
        jjz=orange(1);
        cng=[fpks(k)-kval(jjz);fpks(k)+kval(jjz)];
        if cng(1)<1
            cng(1)=1;
        end
        if cng(2)>length(peakZ(1).fileZ(flecnt).y)
            cng(2)=length(peakZ(1).fileZ(flecnt).y);
        end
        peakZ=slider(peakZ,peakZ(1).fileZ(flecnt).x(cng),flecnt,pkstr,{},-1);

    end
    getpklist=1;

end

if getpklist == 2;
    % uiload brings everything in, we only want the peaklist not the data
end

function [SLIDEVERSION,peakZ]=fullLoad
uiload;

function peakZ=xcelInteg(peakZ)
global MasterPeak;
BASES2N=10;
% pkz={};
OPTIONS=optimset('MaxFunEvals',90000,'MaxIter',800,'Display','off');
tJ=[];
for ij=[1:length(MasterPeak)]
    if MasterPeak(ij).pkvar>0
        % cmName=[mPeak.name];
        %pkz(end+1)={cmName};
        tJ(end+1)=ij;
    end
end
VarPeaks=MasterPeak(tJ);
pkz={MasterPeak(tJ).name};
[s,v] = listdlg('PromptString','Select Peaks to Find:','ListString',pkz);
if v==1
    peakList=VarPeaks(s);
else
    peakList={};
end
for k=[1:length(peakList)];
    cPeak=peakList(k);
    lb=[1000 max([cPeak.pkcenter-cPeak.pkvar,1]) max([cPeak.pkwid-cPeak.widvar,1]) -100 -inf];
    ub=[inf cPeak.pkcenter+cPeak.pkvar cPeak.pkwid+cPeak.widvar 100 inf];
    gParams=[1000 cPeak.pkcenter cPeak.pkwid 0 0];

    peakZ(end+1)=peakZ(end);
    peakZ(end).pkName=cPeak.name;
    peakZ(end).peakStr=75;
    peakZ(end).peakId=cPeak;
    peakZ(end).peakMisc.fitFun='@gaussfun';
    peakZ(end).peakMisc.peakFollow='';
    peakZ(end).peakGroup=-1;
    for flecnt=[1:length(peakZ(1).fileZ)]
        peakZ(end).fileZ(flecnt).x=[];
        peakZ(end).fileZ(flecnt).y=[];
        xMin=lb(2)-cPeak.pkwid;
        xMax=ub(2)+cPeak.pkwid;

        s1=find(abs(peakZ(1).fileZ(flecnt).x-xMin)==min(abs(peakZ(1).fileZ(flecnt).x-xMin)));
        s2=find(abs(peakZ(1).fileZ(flecnt).x-xMax)==min(abs(peakZ(1).fileZ(flecnt).x-xMax)));
        x=peakZ(1).fileZ(flecnt).x(s1:s2);
        y=peakZ(1).fileZ(flecnt).y(s1:s2);
        vecslope=mean(diff(smooth(y,5)))*1/mean(diff(x));
        lb(4)=min([1.5*vecslope,.5*vecslope]);
        ub(4)=max([1.5*vecslope,.5*vecslope]);
        gParams(4)=vecslope;
        gParams(5)=y(1)-gParams(4)*x(1);
        vecslope=gParams(4);
        cydat=(y-y(1))-vecslope*(x-x(1));
        [gParams(1) xCord]=max(cydat);
        gParams(2)=x(xCord);
        for jb=1:5
            [gParamsz,bulldog,resid]=lsqcurvefit(@gaussfun,gParams(1:5),x,smooth(y,20,'sgolay'),lb,ub,OPTIONS);
            plot(x,gaussfun(gParamsz(1:5),x)+flecnt*peakZ(1).plotsep,'r-');
            xMin=gParamsz(2)-gParamsz(3)*4;
            xMax=gParamsz(2)+gParamsz(3)*4;
            peakZ(end).fileZ(flecnt).pkbnd=[xMin xMax];
            s1=find(abs(peakZ(1).fileZ(flecnt).x-xMin)==min(abs(peakZ(1).fileZ(flecnt).x-xMin)));
            s2=find(abs(peakZ(1).fileZ(flecnt).x-xMax)==min(abs(peakZ(1).fileZ(flecnt).x-xMax)));
            x=peakZ(1).fileZ(flecnt).x(s1:s2);
            y=peakZ(1).fileZ(flecnt).y(s1:s2);
            peakZ(end).fileZ(flecnt).pkrange=[s1:s2];
            peakZ(end).fileZ(flecnt).area=pkInt(peakZ(1).fileZ(flecnt),[s1:s2]);
            isignal=gaussfun(gParams(1:5),x);
            signal=gaussfun(gParamsz(1:5),x);
            isolsignal=gaussfun([gParamsz(1:3) 0 0],x);
            noise=abs(y-signal);
            s2n1=((isolsignal).^2)./(noise.^2); % don't count the offset in the signal to noise
            s2n2=sqrt(sum(s2n1));
            s2n3=s2n2/length(noise);
            s2n=20*log(s2n3);
            if s2n>BASES2N/cPeak.visible
                gParams=gParamsz; % feedback the parameters to make the fit get progressively better
            else
                gParams(4)=mean(diff(smooth(y,5)))*1/mean(diff(x));
                vecslope=gParams(4);
                cydat=(y-y(1))-vecslope*(x-x(1));
                [gParams(1) xCord]=max(cydat);
                gParams(2)=x(xCord);
            end
            gParams(5)=y(1)-gParams(4)*x(1);
            disp(['P:' num2str(k) ':SNR #' num2str(flecnt) ' {Gaussian}: {' num2str(round(s2n*10)/10) '}']);
        end

        % disp(['P:' num2str(k) ':SNR #' num2str(flecnt) ' {Gaussian}: {' num2str(round(s2n*10)/10) '}']);
        if s2n>BASES2N/cPeak.visible
            peakZ(end).refData=[y];
            peakZ(end).peakRoot=[xMin xMax];

        else
            gParamsz(1)=0;
        end
        signal=gaussfun(gParamsz(1:5),x);
        hold on;
        disp('Plotting Signal');
        plot(x,signal+flecnt*peakZ(1).plotsep,'k-');
        plot(peakZ(1).fileZ(flecnt).x([s1]),flecnt*peakZ(1).plotsep+peakZ(1).fileZ(flecnt).y([s1]),'bo');
        plot(peakZ(1).fileZ(flecnt).x([s2]),flecnt*peakZ(1).plotsep+peakZ(1).fileZ(flecnt).y([s2]),'b+');
        drawnow;
        peakZ(end).fileZ(flecnt).gParams=[gParamsz s2n];
    end
    pause(.25);
    exportResults(peakZ,1);
end
disp('Done with Batch Peaks!');

function appendMaster(peakZ)
global MasterPeak;
peakslist={};
for k=2:length(peakZ)
    peakslist(end+1)={peakZ(k).pkName};
end
[s,v] = listdlg('PromptString','Select Peaks to Export:','ListString',peakslist);
if v==1
    for jpk=s+1;
        tt=reshape([peakZ(jpk).fileZ.gParams],[7,length(peakZ(1).fileZ)]);
        hw=tt(3,:);
        ret=tt(2,:);
        df=tt(4,:);

        prepArray=[mean(ret) 1.5*std(ret) mean(hw) 1.5*std(hw) 1 mean(df) std(df)];
        if length(ret)<3
            prepArray(2)=.2*prepArray(1);
            prepArray(4)=.2*prepArray(3);
            prepArray(7)=.1*prepArray(6);
        end
        peakZ(jpk).peakId=MasterPeak(writeExcel(prepArray,peakZ(jpk).pkName));
    end
end
function pKelley=writeExcel(prepArray,pkname)
global MasterPeak;
pkz={};
        for ij=[1:length(MasterPeak)]
            pkz(ij)={MasterPeak(ij).name};
        end
        pkz(end+1)={['New Peak...']};
        pKelley= menu(['Select Location for Peak ' pkname ' to go:'],pkz);
        if pKelley<(length(MasterPeak)+1)
            prepArray(2)=MasterPeak(pKelley).pkvar;
            prepArray(4)=MasterPeak(pKelley).widvar;
            
            prepArray(5)=MasterPeak(pKelley).visible;
            prepArray(7)=MasterPeak(pKelley).dfvar;
        end
        if length(dir([pwd '\MasterRetentionList.xls']))>0
            xlswrite([pwd '\MasterRetentionList.xls'],prepArray,['B' num2str(pKelley+1) ':H' num2str(pKelley+1)]);
        else
            xlswrite([matlabroot '\work\slidetools\MasterRetentionList.xls'],prepArray,['B' num2str(pKelley+1) ':H' num2str(pKelley+1)]);
        end
        % MasterPeak(pKelley).name=peakZ(jpk).pkName;
        MasterPeak(pKelley).pkcenter=prepArray(1);
        MasterPeak(pKelley).pkvar=prepArray(2);
        MasterPeak(pKelley).pkwid=prepArray(3);
        MasterPeak(pKelley).widvar=prepArray(4);
        MasterPeak(pKelley).visible=prepArray(5);
        MasterPeak(pKelley).df=prepArray(6);
        MasterPeak(pKelley).dfvar=prepArray(7);
        
        
function porder=peaklistOrder(namez,followz)

porder=[];
dnPkz={''};
stillRunning=1;
pkZleft=[1:length(namez)];
while stillRunning
    for i=pkZleft
        for j=[1:length(dnPkz)]
            if strcmp(cell2mat(followz(i)),cell2mat(dnPkz(j)))
                porder(end+1)=i;
            end
        end
    end
    stillRunning=0;
    for i=porder

        if sum(pkZleft==i)>0
            pkZleft(find(pkZleft==i))=[];
            dnPkz(end+1)=namez(i);
            stillRunning=1;
        end
    end
end
pkZleft=[1:length(namez)];
pkZleft(porder)=[];
porder=[porder pkZleft];
namez(porder)



