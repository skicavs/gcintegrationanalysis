/*=================================================================
* jaque
* This example demonstrates how to populate a sparse
* matrix.  For the purpose of this example, you must pass in a
* non-sparse 2-dimensional argument of type double.

* Comment: You might want to modify this MEX-file so that you can use
* it to read large sparse data sets into MATLAB.
*
* This is a MEX-file for MATLAB.  
* Copyright 1984-2006 The MathWorks, Inc.
* All rights reserved.
*=================================================================*/

/* $Revision: 1.5.6.2 $ */

#include <math.h> /* Needed for the ceil() prototype */
#include <stdio.h>
#include "mex.h"

/* If you are using a compiler that equates NaN to be zero, you must
 * compile this example using the flag  -DNAN_EQUALS_ZERO. For example:
 *
 *     mex -DNAN_EQUALS_ZERO fulltosparse.c
 *
 * This will correctly define the IsNonZero macro for your C compiler.
 */

#if defined(NAN_EQUALS_ZERO)
#define IsNonZero(d) ((d)!=0.0 || mxIsNaN(d))
#else
#define IsNonZero(d) ((d)!=0.0)
#endif
// jaque(refpeak,cgraph,rng,ns,boundZ)

void mexFunction(
		 int nlhs,       mxArray *plhs[],
		 int nrhs, const mxArray *prhs[]
		 )
{
    /* Declare variable */
    
    mwSize rngLen,refLen;
    //mwIndex *irs,*jcs;
    mxArray *cpeak,*jArr,*dim_array;
    int cmplx,isfull,curMinDex,n,i,k,nsdown,nsup;
    double *refpeak,*cgraph,*rng,*ns,*boundZ,curMax,*cpeakR,peakMax,scaleF,curSum,curMin,*jRea,*outWrite;
    double percent_sparse;
    
    /* Check for proper number of input and output arguments */    
    if (nrhs != 5) {
	mexErrMsgTxt("5 input arguments required.");
    } 
    if(nlhs !=3){
	mexErrMsgTxt("Too many output arguments.");
    }
    
    /* Check data type of input argument  */
    if (!(mxIsDouble(prhs[0]))){
	mexErrMsgTxt("Input argument must be of type double.");
    }	
    
    if (mxGetNumberOfDimensions(prhs[0]) != 2){
	mexErrMsgTxt("Input argument must be two dimensional\n");
    }

    /* Get the size and pointers to input data */
    //m  = mxGetM(prhs[0]);
    //n  = mxGetN(prhs[0]);
    
    //pr = mxGetPr(prhs[0]);
    //pi = mxGetPi(prhs[0]);
    refpeak=mxGetPr(prhs[0]);
    refLen=mxGetN(prhs[0]);
    cgraph=mxGetPr(prhs[1]);
    rng=mxGetPr(prhs[2]);
    rngLen = mxGetM(prhs[2]);
    dim_array=mxGetDimensions(prhs[2]);
    ns = mxGetPr(prhs[3]); 
    boundZ=mxGetPr(prhs[4]);
    nsdown=ns[0];
    nsup=ns[0];
    printf("%f\n\n",dim_array[0]);
    if ((rng[0]-nsdown)<boundZ[0]) {
        nsdown=abs(boundZ[0]-rng[0]);
    }
    if ((rng[rngLen]+nsup)>boundZ[1]) {
        nsup=boundZ[1]-rng[rngLen]-1;
    }
    
    
    curMax=0;
    curMin=0;
    curMinDex=-1;
    for (n=0;n<nsup;n++) {
        refpeak[n]=refpeak[n]-refpeak[0];
        if (refpeak[n]>refpeak[n]) curMax=refpeak[n];
    }
    cpeak = mxCreateDoubleMatrix(1,rngLen,mxREAL);
    cpeakR=mxGetPr(cpeak);
    jArr = mxCreateDoubleMatrix(1,nsup+nsdown,mxREAL);
    jRea=mxGetPr(jArr);
    for (n=-nsdown;n<nsup;n++) {
        peakMax=0;
        for (i=0;i<rngLen;i++) {
            cpeakR[i]=cgraph[i+n]-cgraph[n];
            if (cpeakR[i]>peakMax) peakMax=cpeakR[i];
        }
        scaleF=curMax/peakMax;
        if (scaleF<.1) scaleF=0.1;
        if (scaleF>10) scaleF=10;
        curSum=0;
        for (k=0;k<rngLen;k++) {
            curSum+=pow(refpeak[k]-cpeakR[k],2);
            
        }
        jRea[n+nsdown]=sqrt(curSum/rngLen);
        if (curMinDex==-1) {
             curMin=jRea[n+nsdown];
             curMinDex=n+nsdown;
        }
        if (curMin>jRea[n+nsdown]) {
            curMin=jRea[n+nsdown];
            curMinDex=n+nsdown;
        }
        printf("%i %f\n",n,jRea[n+nsdown]);
    }
    plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
    outWrite=mxGetPr(plhs[0]);
    outWrite[0]=curMinDex;
    //mxSetPr(plhs[0],curMinDex);
    //mxSetPr(nlhs[1],jed);
    //mxSetPr(nlhs[2],snr);
}
/*
if isempty(j)
    shft=0;
    resid=1e9;
    jed.jval=1e10;
    jed.offset=0;
else
    [alber shft]=min([j.jval]);
    resid=j(shft).jval;
    shft=j(shft).offset;
    jed=j;
end
if resid==0
    resid=0.1;
end
snr=20*log(sqrt(sum(refpeak.^2)/length(refpeak))/resid);
 */
