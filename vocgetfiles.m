function [selectedfiles,isthere] = vocgetfiles
%   vocgetfiles - load the gc data files from current directory
%
%   SYNOPSIS:   selectedfiles = getgcfiles
%               selectedfiles = getgcfiles('filefilter')
%               
%   INPUT:      filefilter - a string such as '*.txt' or '*.dat', the default filter is '*.asc'
%               minbytes - minimum size of desired data files(used as filter)
%
%   OUTPUT:     selectedfiles - the files the user has selected from the dialog box
%
%   NOTES:
%

% % check to see if a different filter is specified, default to '*.ASC' & '*.VOC'
% if nargin == 0;
%     filefilter.legacy = '*.asc';
%     filefilter.proto='*.voc';
%     minbytes = 0;
% end
% [filename, pathname] = uigetfile({'*.voc','VOC Files (Newer Machines)';'*.asc','ASC Files (Older Machines)'},'Please select the files for peaklists','Multiselect','On');
[filename, pathname] = uigetfiles('*.voc;*.asc','Please Select Files for Peaklist');

selectedfiles = strcat(pathname,filename);
if iscell(selectedfiles)
    selectedfiles=sort(selectedfiles);
else
    selectedfiles={selectedfiles};
end
isthere=1;