function time=ktime(filename)
filename=upper(filename);
b=regexp(filename,'.*?(?<filename>\w*[.]\w*)','names');
filename=b(end).filename; % gets rid of all the directory nonsense
assumeYear=2007;
if strcmp(filename(end-2:end),'ASC') % ASC formatted file
elseif strcmp(filename(end-2:end),'VOC')
    monthTable=['1':'9' 'OND'];
    hrTable=['A':'Z'];
    %find(monthTable==filename(2));
    time=datenum(assumeYear,find(monthTable==filename(2)),str2num(filename(3:4)),find(hrTable==filename(5)),str2num(filename(6:7)),0);
end


