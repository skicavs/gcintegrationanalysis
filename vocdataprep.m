function [data, hdrinfo,time] = vocdataprep(selectedfiles,plots)

% vocdataprep - prepare the data files for evaluation and plotting
%
% SYNOPSIS:     [data, hdrinfo] = vocdataprep(selectedfiles)
%
% INPUT:        selectedfiles - list of files chosen to be plotted, etc
%               plots - 1 for yes (show plots), 0 for no plots
%
% OUTPUT:       data - range change adjusted data
%               hdrinfo - structure of header info
%
% NOTES:

% if nargin < 1
%   error('Function requires one input argument');
%
% end

for L = 1:length(selectedfiles);

    if L == 1;
        timesum = 0;
    end

    % load the datafile and header info
    [header, data] = vocload(char(selectedfiles(L)));

    %determine what kind of machine produced the data
    currentFile = char(selectedfiles(L));

    isproto = lower(currentFile(end-1))=='o';
    islegacy = lower(currentFile(end-1))=='s';

    if isproto == 1
        hdrinfo(L) = vocheadersProto(header);

        if size(data,2) == 2
            time = data(:,1);
            data = data(:,2);
        elseif size(data,2)==3
            time=data(:,1);
            data(:,1)=[];
        else
            hStr=upper(reshape(header',1,size(header,1)*size(header,2)));
            et1=regexp(hStr,'<\s*?TOTAL[^>]*?SAMPLE[^>]*?TIME.*?>=\s*(?<eltime>\d*[.]?\d*)','names');
            if isempty(et1)
                et1=regexp(hStr,'<\s*?RUN[^>]*?TIME.*?>=\s*(?<eltime>\d*[.]?\d*)','names');
            end
            if isempty(et1)
                eltime=size(data,1)/60;
            else
                eltime=str2num(et1.eltime)/60;
            end
            time = (1:size(data,1))% .*eltime/size(data,1);
            time = time(:);
        end


        % rchange = find(time<str2num(hdrinfo(L).rangechange));
        % data = vocdropfix(data,rchange(end,:));

        if plots == 1
            % get info about data from headers
            astart = findstr(hdrinfo(L).columns,'A->');
            bstart = findstr(hdrinfo(L).columns,'B->');
            columnA = deblank(hdrinfo(L).columns(astart:end));

            timecode = voctimecode(char(selectedfiles(L)));

            % show a plot in new window
            figure
            plot(time/60,data, 'g');
            title(strcat(hdrinfo(L).date,'  @',timecode));
            xlabel('Time, minutes');
            ylabel('Intensity');
            text(time(end,1)/60,data(end,1),columnA);
        end

    elseif islegacy == 1
        hdrinfo(L) = vocheaders(header);

        %dsize(L,:) = size(data);

        % fix the rangechange blip if factor is greater than 1
        % junky code
        %     if str2num(hdrinfo(L).rangechange) ~= 1;
        %         if size(data,2) == 2;
        %             data(:,1) = vocdropfix(data(:,1),str2num(hdrinfo(L).rangechange));
        %             data(:,2) = vocdropfix(data(:,2),str2num(hdrinfo(L).rangechange));
        %
        %         elseif size(data,2) == 1;
        %             data = vocdropfix(data,str2num(hdrinfo(L).rangechange));
        %
        %         end
        %
        %     end


        % make data column for elapsed time
        % eltime = str2num(strtok(hdrinfo(L).notes2))/60;
        % ze dont trust the headers time for regular expressions aha
        hStr=upper(reshape(header',1,size(header,1)*size(header,2)));
        et1=regexp(hStr,'<\s*?TOTAL[^>]*?SAMPLE[^>]*?TIME.*?>=\s*(?<eltime>\d*[.]?\d*)','names');
        if isempty(et1)
            et1=regexp(hStr,'<\s*?RUN[^>]*?TIME.*?>=\s*(?<eltime>\d*[.]?\d*)','names');
        end
        if isempty(et1)
            eltime=size(data,1)/60;
        else
            eltime=str2num(et1.eltime)/60;
        end

        time = (1:size(data,1)).*eltime/size(data,1);
        time=time(:);
        timesum = timesum + time;

        % plot only if specified
        if plots == 1 ;
            % get info about data from headers
            astart = findstr(hdrinfo(L).columns,'A->');
            bstart = findstr(hdrinfo(L).columns,'B->');
            columnA = deblank(hdrinfo(L).columns(astart:bstart-1));
            timecode = voctimecode(char(selectedfiles(L)));

            % show a plot in new window with same colors as tekbasic plots
            figure
            plot(time,data(:,1), 'g');
            title(strcat(hdrinfo(L).date,'  @',timecode));
            xlabel('Time, minutes');
            ylabel('Intensity');
            text(.75*time(end,1),data(end,1),columnA);

            if size(data,2) == 2;
                % find info on column type
                bstart = findstr(hdrinfo(L).columns,'B->');
                columnB = deblank(hdrinfo(L).columns(bstart:end));

                hold on
                plot(time,data(:,2),'b');
                text(.75*time(end,1),data(end,2),columnB);

                hold off

            end

        end

    end

end %islegacy





% compute average time of chromatogram
%avgtime = timesum./length(selectedfiles);