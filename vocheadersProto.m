function hdrinfo = vocheadersProto(header)

% vocheaders - extract important header info from VOCTEC chromatogram data files
%
% SYNOPSIS:     hdrinfo = vocheaders(header)
%
% INPUT:        header - header strings created by vocload
%
% OUTPUT:       hdrinfo - structure containing important header info that is
%                         used for data reduction, etc
%               
% NOTES:        the data files must be standard VOCTEC files
%

% check number of and type of input arguments

if nargin < 1;
  error('Function requires one input argument');
elseif ~ischar(header);
    error('Headers must be character array');
end

% how many rows of headers are there
nrows = size(header,1);

% find where the equal signs are in all the header rows

% make exceptions for the last two rows of notes
eqloc = [];


for eq = 2:nrows;
    
    loc = findstr(header(eq,:),'=') + 1;
        
        t = 1;
        
        % get rid of spaces before info
        while isspace(header(eq,loc)) & t < 15;
            loc = loc + 1;
            t = t + 1;
            
        end
        
        if eq == 9;
            gases = loc;
        elseif eq == 11
            clocktime = loc;
        elseif eq == 15
            rangechange2 = loc;
        else
            eqloc = [eqloc loc];
            
        end   
end

% create structure of string arrays containing important header info
hdrinfo.date = deblank(header(2,eqloc(:,1):end));
hdrinfo.lab = deblank(header(3,eqloc(:,2):end));
hdrinfo.client = deblank(header(4,eqloc(:,3):end));
hdrinfo.location = deblank(header(5,eqloc(:,4):end));
hdrinfo.samptype = deblank(header(6,eqloc(:,5):end));
hdrinfo.instrument = deblank(header(7,eqloc(:,6):end));
hdrinfo.columns = deblank(header(8,eqloc(:,7):end));

%there are two sets of info on this header line
hdrinfo.carrier = deblank(header(9,gases(:,1):gases(:,2)-9));
hdrinfo.fuel = deblank(header(9,gases(:,2):end));

hdrinfo.adconv = deblank(header(10,eqloc(:,8):end));

%there are two sets of info on this header line
hdrinfo.clocktime = deblank(header(11,clocktime(:,1):clocktime(:,2)-21));
hdrinfo.ptsavg = deblank(header(11,clocktime(:,2):end));

hdrinfo.temprog = deblank(header(12,eqloc(:,9):end));
hdrinfo.ranges = deblank(header(13,eqloc(:,10):end));
hdrinfo.rangefact = deblank(header(14,eqloc(:,11):end));

%there are two sets of info on this header line
hdrinfo.rangechange = deblank(header(15,rangechange2(:,1):rangechange2(:,2)-23));
hdrinfo.rangechange2 = deblank(header(15,rangechange2(:,2):end));

hdrinfo.vswitch = deblank(header(16,eqloc(:,12):end));
hdrinfo.numdatpts = deblank(header(17,eqloc(:,13):end));
hdrinfo.cooltime = deblank(header(18,eqloc(:,14):end));
hdrinfo.sampinfo = deblank(header(19,eqloc(:,15):end));
hdrinfo.notes1 = deblank(header(20,eqloc(:,16):end));
hdrinfo.notes2 = deblank(header(21,eqloc(:,17):end));

