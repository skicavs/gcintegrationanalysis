function [summits]=summit(ydata,minh,mind)
summits={};
for i=1:length(ydata)
    if ydata(i)>minh
        if ~isempty(summits)
            if (i-summits(end).pkloc)>mind
                summits(end+1).pkloc=i;
                summits(end).pkheight=ydata(i);
            elseif summits(end).pkheight<ydata(i)
                summits(end).pkloc=i;
                summits(end).pkheight=ydata(i);
            end
        else
            summits(end+1).pkloc=i;
            summits(end).pkheight=ydata(i);
        end
    end
end