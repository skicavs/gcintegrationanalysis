function integrate(filename,compound,placement,conversion)
peaklist=[];i=1;
m=csvread(filename,0,3) %load up all the data
peaklist=m(1,:) %get just the peaklist

[rows columns] = size(m);

while i<length(peaklist)
    if abs(peaklist(i)-placement) < 9 % found match!
       fprintf('Found a match at %d when i=%d.\n\n',peaklist(i),i);
       break;
    else
        i=i+1;    
    end
end
fprintf('Date/Time\t\t\tProportions (in ppm)\n');
fprintf('\t\t\t\t\t%s',compound);
%have it print all the data according to the timecode in columns of the compound
[dayoweek, dayt] = strtok(hdrinfo(j).date,',');

        dateloc = findstr(dayt,'-');
        dayt = dayt(dateloc(1)-2:dateloc(end));
        time = voctimecode(char(selectedfiles(j)));
        dayt =  strcat(dayt,time);
end