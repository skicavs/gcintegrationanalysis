function dealKey(key)
global linehandles;
global LegendLabels
global lstpeak;
global manu;
global baroJump;
global SlideFigure;
global baroSTOP;
updatePlot=0;
if key==28
    lstpeak=lstpeak-10;
    updatePlot=1;
elseif key==29
    lstpeak=lstpeak+10;
    updatePlot=1;
elseif key==30
    lstpeak=lstpeak+1;
    updatePlot=1;
elseif key==31
    lstpeak=lstpeak-1;
    updatePlot=1;
elseif key==98 | key==66
    lstpeak=1;
    updatePlot=1;
elseif key==116 | key==84
    lstpeak=length(linehandles);
    updatePlot=1;
elseif key==109
    lstpeak=round(length(linehandles)/2);
    updatePlot=1;
elseif key==13
    manu=1;
elseif key==3
    manu=1;
elseif key==8
    manu=9989;
elseif key==115
    %manu=9;
    baroSTOP=1;

elseif key==83
    %manu=9;
    baroSTOP=1;
elseif key==113
    manu=-1
elseif key==81
    manu=-1
elseif key==107 | key==75
    % keyboard/debug mode
    manu=6969;
elseif key==106 | key==74
    lstpeak=lstpeak+baroJump;
    baroJump=0;
    updatePlot=1;
elseif key==45
    baroJump=-1*baroJump;
elseif length(str2num(char(key)))>0
    baroJump=baroJump*10+str2num(char(key));
elseif key==-1
    updatePlot=1;
else
    disp(num2str(key));
end

if updatePlot
    set(linehandles,'LineStyle','-');
    set(linehandles,'Marker','none');
    %disp(num2str(key));
    legVars={};
    try
        k=legend;
        legVars=get(k);
    catch
        disp('Cannot Read Legend')
    end

    if ~isempty(legVars)
        
        %set(k)
        if length(linehandles)>25
            lgShow=lstpeak+10:-1:lstpeak-10;
            lgShow=mod(lgShow-1,length(linehandles))+1;
            legend(linehandles(lgShow),LegendLabels(lgShow),'Interpreter','none');
        else
            legend(linehandles(end:-1:1),LegendLabels(end:-1:1),'Interpreter','none');
        end
        k=legend;
        set(k,'Visible',legVars.Visible);
        set(k,'Location',legVars.Location);
        set(k,'Orientation',legVars.Orientation);
        set(k,'Position',legVars.Position);
        
    end
    lstpeak=mod(lstpeak-1,length(linehandles))+1;
    set(linehandles(lstpeak),'Marker','.');
    if SlideFigure>-1
        figure(SlideFigure);
        title(['Chromatogram #' num2str(lstpeak)]);
    end
end
