function slidemovie
disp('Now Loading Data...');
uiload;
close all;
figure
snope=menu('Choose output format...','AVI','JPG');
if snope==2 
    cpr=cell2mat(inputdlg('Enter directory name..','User Input Required'))
    eval(['mkdir ' cpr]);
    eval(['cd ' cpr]);
end

for k=[2:length(peakZ)]
    for j=[1:length(peakZ(1).fileZ)]
        cPeak=peakZ(k).fileZ(j);
        x1=min(cPeak.pkbnd);
        x2=max(cPeak.pkbnd);
        ss1=find(abs(peakZ(1).fileZ(j).x-x1)==min(abs(peakZ(1).fileZ(j).x-x1)));
        ss2=find(abs(peakZ(1).fileZ(j).x-x2)==min(abs(peakZ(1).fileZ(j).x-x2)));
        ss1=ss1(1);
        ss2=ss2(1);
        xdata=peakZ(1).fileZ(j).x(ss1:ss2);
        ydata=peakZ(1).fileZ(j).y(ss1:ss2);
        gUnit=peakZ(k).fileZ(j).gParams;
        gdata=gaussfun(gUnit,xdata);
        sldata=gaussfun([0 0 0 gUnit(4) gUnit(5)],xdata);
        hold off;
        plot(xdata,ydata,'r+');
        hold on;
        plot(xdata,gdata,'b-');
        plot(xdata,sldata,'g.');
        fname=[str2mat(peakZ(1).fileZ(j).name) '-P' num2str((x1+x2)/2) 'C' num2str(j)];
        title([fname  '.pks']);
        if snope==1
            xvideo((k-2)*length(peakZ(1).fileZ)+j)=getframe(gcf);
        elseif snope==2
            pbj=find(fname=='\');
            saveas(gcf,[fname(pbj(end)+1:end) '.jpg'],'jpg');
        end
    end
end
if snope==1
[filename, pathname, filterindex] = uiputfile({'*.avi',  'Audio Video Interleave (*.avi)'},'Save','peakZ.avi');
disp('Now Saving Movie');
movie2avi(xvideo,[pathname filename],'FPS',1.5,'QUALITY',25);
end

function f=gaussfun(A,x)
f = A(1)*exp(-0.5.*((x-A(2))/A(3)).^2)+A(4)*x+A(5);