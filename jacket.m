function jacket()
flelist=dir('*.voc');
[gb src]=vocload(flelist(1).name);
sjkt=src(:,1);
jkt=src(:,2);
hold off
plot(sjkt,jkt);
hold on
pause(10);
[x1,y1]=ginput(1);
[x2,y2]=ginput(1);
s1=find(abs(sjkt-x1)==min(abs(sjkt-x1)));
s2=find(abs(sjkt-x2)==min(abs(sjkt-x2)));
plot(x1,y1,'r+')
plot(x2,y2,'r+')
s3=s2-s1;
jkt=jkt([s1:s1+s3]);
bjkt=jkt;
n=2;
fcknug=[];
while n<=length(flelist)
   % figure
    [gb srcs]=vocload(flelist(n).name);
    % fcknug=[fcknug [1:rand*500]];
    sjkt2=[fcknug'; srcs(:,1)];
    jkt2=[srcs(:,2); fcknug' ];
    plot(sjkt2,n*10000+jkt2);
    ss1=find(abs(sjkt2-x1)==min(abs(sjkt2-x1)));
    ss2=find(abs(sjkt2-x2)==min(abs(sjkt2-x2)));
    cng=[ss1:ss1+s3];
    shft=jaque(jkt,jkt2,cng,1000)
    c1=[sjkt2(ss1+shft) sjkt2(ss1+shft)];
    c2=[jkt2(ss1+shft) n*10000+jkt2(ss1+shft)];
    plot(c1(2),c2(2),'r+')
   % line(c1,c2);
    c1=[sjkt2(ss1+s3+shft) sjkt2(ss1+s3+shft)];
    c2=[jkt2(ss1+s3+shft) n*10000+jkt2(ss1+s3+shft)];
   % line(c1,c2);
    plot(c1(2),c2(2),'r+')
    pause(.1)
    jkt=jkt2(cng+shft);
    cng=cng+shft;
    if shft==0
        jkt=bjkt;
    end
    x1=sjkt2(cng(1)+shft);
    x2=sjkt2(cng(end)+shft);
    n=n+1;
end
   
function shft=jaque(jkt,jkt2,rng,ns)
n=-ns;
jkt=jkt-jkt(1);
j=[];
%figure
while n<=ns
jkt3=jkt2(rng+n);
jkt3=jkt3-jkt3(1);
jkt3=max(jkt)/max(jkt3)*jkt3;
j=[j sum(abs(jkt-jkt3))];
%plot(jkt)
%hold on
%plot(jkt3)
%hold off
%pause(.001)
n=n+1;
end
%close
%j
[alber shft]=min(j);
shft=shft-ns;
if (sum(jkt.^2)/sum(alber.^2))<1 
    shft=0;
end
