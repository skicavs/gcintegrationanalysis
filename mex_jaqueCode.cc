#include <octave/oct.h>

extern "C" {
  // mex.cc names both mexFunction (c) and MEXFUNCTION (Fortran)
  // but the mex file only defines one of them, so define the other
  // here just to keep the linker happy, but don't ever call it.
  void F77_FUNC(mexfunction,MEXFUNCTION)() {}
  const char *mexFunctionName = "jaqueCode";
} ;

DEFUN_DLD(jaqueCode, args, nargout,
"jaqueCode not directly documented. Try the following:\n   type(file_in_loadpath('jaqueCode.m'))\n")
{
  octave_value_list C_mex(const octave_value_list &, const int);
  return C_mex(args, nargout);
}
