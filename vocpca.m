function [cellz,pc,score,latent]=vocpca()
d=1;
cellz={};
data=[];
cname={};
while d==1
    file=uigetfile({'*.csv;*.csv','SlideTools Output'});
    [cfdata,cname]=loadumx(file,cname);
    cellz{end+1}.data=cfdata;
    cellz{end}.name=file;
    data=[data; cfdata;];
    
    d=menu('Load another file?','No','Yes')-1;
end
[pc,score,latent]=princomp(data);

function [data,cname]=loadumx(file,iname)

fid = fopen(file, 'r');
data=[];
cname={};
dd1=fgetl(fid);
cmz=find(dd1==',');

dd1=dd1(cmz(1)+1:end);
str1= char(dd1);
cmz=[0 find(dd1==',')];
for j=1:(length(cmz)-1)
    cname{j}=dd1((cmz(j)+1):(cmz(j+1)-1));
end
while ~feof(fid)
    dd1=fgetl(fid);
    cmz=find(dd1==',');
    dd1=dd1(cmz(1)+1:end);
    str1= char(dd1);
    [ndata, ncols, errmsg, nxtindex] = sscanf(str1, '%f,');
    data=[data;ndata'];
end
data(:,15)=[];


fclose(fid);
if isempty(iname)
    [s,v] = listdlg('PromptString','Select Columns to use:','ListString',cname);
    if v==1
        data=data(:,s);
        cname=cname(s);
    end
else
    if length(cname)>=length(iname)
        transMat=[];
        for ij=1:length(iname)
            isFound=0;
            ik=1;
            while ~isFound
                if ~isFound & ik>length(cname)
                    isFound=1;
                    transMat(end+1)=menu(['Select peak in this file that matches ' iname{ij} ':'],cname);

                else
                    if strcmp(cname{ik},iname{ij})
                        isFound=1;
                        transMat(end+1)=ik;
                    end
                end
                ik=ik+1;
            end
        end
        cname=cname(transMat);
        data=data(:,transMat);
    else
        error('data too short');
    end
end

figure;
plot(data);
title(file);
legend(cname);
